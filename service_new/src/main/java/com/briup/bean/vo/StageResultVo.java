package com.briup.bean.vo;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stage_result_vo")
public class StageResultVo {

    @Id
    private Long id;

    //关卡id
    private String stageId;

    //章节名称
    private String chapterName;

    //章节内关卡编号
    private Integer code;

    //产物ID
    private String itemId;

    // 关卡名称
    private String stageName;

    // 关卡英文名称
    private String stageEnName;

    // 样本次数
    private Integer times;

    // 概率
    private Double probability;

    // 概率
    private Double requset;

    // 期望理智
    private Double expect;

    // 主产物
    private String main;

    //材料类型
    private String type;

    // 副产物
    private String secondary;

    private String secondaryId;

    //效率
    private Double efficiency;

    // 颜色
    private Integer color;

    //相对效率百分比
    private Double percentage;

    //更新时间
    private String updateDate;

    //每分钟消耗理智
    private String spm;

    private  Integer isUseValue;

    // 截止时间
    private Integer isShow;

    private  String activityName;

    private String extraItem;

    private Double efficiencyEx;

    private String confidence;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStageId() {
        return stageId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public String getStageEnName() {
        return stageEnName;
    }

    public void setStageEnName(String stageEnName) {
        this.stageEnName = stageEnName;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    public Double getProbability() {
        return probability;
    }

    public void setProbability(Double probability) {
        this.probability = probability;
    }

    public Double getExpect() {
        return expect;
    }

    public void setExpect(Double expect) {
        this.expect = expect;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSecondary() {
        return secondary;
    }

    public void setSecondary(String secondary) {
        this.secondary = secondary;
    }

    public String getSecondaryId() {
        return secondaryId;
    }

    public void setSecondaryId(String secondaryId) {
        this.secondaryId = secondaryId;
    }

    public Double getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(Double efficiency) {
        this.efficiency = efficiency;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getSpm() {
        return spm;
    }

    public void setSpm(String spm) {
        this.spm = spm;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getIsUseValue() {
        return isUseValue;
    }

    public void setIsUseValue(Integer isUseValue) {
        this.isUseValue = isUseValue;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Double getRequset() {
        return requset;
    }

    public void setRequset(Double requset) {
        this.requset = requset;
    }

    public String getExtraItem() {
        return extraItem;
    }
    public void setExtraItem(String extraItem) {
        this.extraItem = extraItem;
    }

    public Double getEfficiencyEx() {
        return efficiencyEx;
    }

    public void setEfficiencyEx(Double efficiencyEx) {
        this.efficiencyEx = efficiencyEx;
    }

    public String getConfidence() {
        return confidence;
    }

    public void setConfidence(String confidence) {
        this.confidence = confidence;
    }

    @Override
    public String toString() {
        return "StageResultVo{" +
                "id=" + id +
                ", stageId='" + stageId + '\'' +
                ", chapterName='" + chapterName + '\'' +
                ", code=" + code +
                ", itemId='" + itemId + '\'' +
                ", stageName='" + stageName + '\'' +
                ", stageEnName='" + stageEnName + '\'' +
                ", times=" + times +
                ", probability=" + probability +
                ", requset=" + requset +
                ", expect=" + expect +
                ", main='" + main + '\'' +
                ", type='" + type + '\'' +
                ", secondary='" + secondary + '\'' +
                ", secondaryId='" + secondaryId + '\'' +
                ", efficiency=" + efficiency +
                ", color=" + color +
                ", percentage=" + percentage +
                ", updateDate='" + updateDate + '\'' +
                ", spm='" + spm + '\'' +
                ", isUseValue=" + isUseValue +
                ", isShow=" + isShow +
                ", activityName='" + activityName + '\'' +
                ", extraItem='" + extraItem + '\'' +
                ", efficiencyEx=" + efficiencyEx +
                ", confidence='" + confidence + '\'' +
                '}';
    }
}
