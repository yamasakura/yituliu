package com.briup.bean.vo;

import com.alibaba.excel.annotation.ExcelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;



public class StageVo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String stageName;

	private String stageEnName;

	private String stageId;

	private String chapterName;

	private Integer chapterCode;

	private Double reason;

	private Double reasonEx;

	private String main;

	private String secondary;

	private String secondaryId;

	private Double spm;

	private String type;
	// 判断是否开放

	private Integer isOpen;
	// 判断是否有限时掉落

	private Integer isSpecial;

	private Integer isValue;

	private Integer isShow;

	private String stageType;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public String getStageEnName() {
		return stageEnName;
	}

	public void setStageEnName(String stageEnName) {
		this.stageEnName = stageEnName;
	}

	public String getStageId() {
		return stageId;
	}

	public void setStageId(String stageId) {
		this.stageId = stageId;
	}

	public String getChapterName() {
		return chapterName;
	}

	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}

	public Integer getChapterCode() {
		return chapterCode;
	}

	public void setChapterCode(Integer chapterCode) {
		this.chapterCode = chapterCode;
	}

	public Double getReason() {
		return reason;
	}

	public void setReason(Double reason) {
		this.reason = reason;
	}

	public String getMain() {
		return main;
	}

	public void setMain(String main) {
		this.main = main;
	}

	public String getSecondary() {
		return secondary;
	}

	public void setSecondary(String secondary) {
		this.secondary = secondary;
	}

	public String getSecondaryId() {
		return secondaryId;
	}

	public void setSecondaryId(String secondaryId) {
		this.secondaryId = secondaryId;
	}

	public Double getSpm() {
		return spm;
	}

	public void setSpm(Double spm) {
		this.spm = spm;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(Integer isOpen) {
		this.isOpen = isOpen;
	}

	public Integer getIsSpecial() {
		return isSpecial;
	}

	public void setIsSpecial(Integer isSpecial) {
		this.isSpecial = isSpecial;
	}

	public Integer getIsValue() {
		return isValue;
	}

	public void setIsValue(Integer isValue) {
		this.isValue = isValue;
	}

	public Integer getIsShow() {
		return isShow;
	}

	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}

	public String getStageType() {
		return stageType;
	}

	public void setStageType(String stageType) {
		this.stageType = stageType;
	}

	public Double getReasonEx() {
		return reasonEx;
	}

	public void setReasonEx(Double reasonEx) {
		this.reasonEx = reasonEx;
	}

	@Override
	public String toString() {
		return "StageVo{" +
				"stageName='" + stageName + '\'' +
				", stageEnName='" + stageEnName + '\'' +
				", stageId='" + stageId + '\'' +
				", chapterName='" + chapterName + '\'' +
				", chapterCode=" + chapterCode +
				", reason=" + reason +
				", reasonEx=" + reasonEx +
				", main='" + main + '\'' +
				", secondary='" + secondary + '\'' +
				", secondaryId='" + secondaryId + '\'' +
				", spm=" + spm +
				", type='" + type + '\'' +
				", isOpen=" + isOpen +
				", isSpecial=" + isSpecial +
				", isValue=" + isValue +
				", isShow=" + isShow +
				", stageType='" + stageType + '\'' +
				'}';
	}
}
