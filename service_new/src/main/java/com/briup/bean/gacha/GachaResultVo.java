package com.briup.bean.gacha;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gacha_result_vo")
public class GachaResultVo {


    @Id
    private String tsUid;
    private String ts;
    private Integer times;
    private String pool;
    private String name;
    private Integer rarity;
    private boolean isNew;




    public String getTsUid() {
        return tsUid;
    }

    public void setTsUid(String tsUid) {
        this.tsUid = tsUid;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    public String getPool() {
        return pool;
    }

    public void setPool(String pool) {
        this.pool = pool;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRarity() {
        return rarity;
    }

    public void setRarity(Integer rarity) {
        this.rarity = rarity;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    @Override
    public String toString() {
        return "GachaResultVo{" +
                "tsUid='" + tsUid + '\'' +
                ", ts='" + ts + '\'' +
                ", times=" + times +
                ", pool='" + pool + '\'' +
                ", name='" + name + '\'' +
                ", rarity=" + rarity +
                ", isNew=" + isNew +
                '}';
    }
}
