package com.briup.bean.gacha;

public class JsonRep {

    private Integer appId;

    private Integer channelMasterId;

    private ChannelToken channelToken;

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public Integer getChannelMasterId() {
        return channelMasterId;
    }

    public void setChannelMasterId(Integer channelMasterId) {
        this.channelMasterId = channelMasterId;
    }

    public ChannelToken getChannelToken() {
        return channelToken;
    }

    public void setChannelToken(ChannelToken channelToken) {
        this.channelToken = channelToken;
    }

    @Override
    public String toString() {
        return "JsonRep{" +
                "appId=" + appId +
                ", channelMasterId=" + channelMasterId +
                ", channelToken=" + channelToken +
                '}';
    }
}


