package com.briup.bean.gacha;

public class ChannelToken{
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ChannelToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "ChannelToken{" +
                "token='" + token + '\'' +
                '}';
    }
}
