package com.briup.bean.gacha;

public class MutableInteger {

    private int val;

    public MutableInteger(int val) {
        this.val = val;
    }

    public int get() {
        return val;
    }

    public void set(int val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return "MutableInteger{" +
                "val=" + val +
                '}';
    }
}