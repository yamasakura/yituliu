package com.briup.bean.gacha;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gacha_data")
public class GachaData {



    @Id
    private String gachaId;
    private String ts;
    private String pool;
    private String name;
    private Integer rarity;
    private boolean isNew;





    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getGachaId() {
        return gachaId;
    }

    public void setGachaId(String gachaId) {
        this.gachaId = gachaId;
    }

    public String getPool() {
        return pool;
    }

    public void setPool(String pool) {
        this.pool = pool;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRarity() {
        return rarity;
    }

    public void setRarity(Integer rarity) {
        this.rarity = rarity;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    @Override
    public String toString() {
        return "GachaData{" +
                "gachaId='" + gachaId + '\'' +
                ", ts='" + ts + '\'' +
                ", pool='" + pool + '\'' +
                ", name='" + name + '\'' +
                ", rarity=" + rarity +
                ", isNew=" + isNew +
                '}';
    }
}
