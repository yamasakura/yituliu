package com.briup.bean.gacha;

public class PoolInfo {


    private String pool;
    private Integer lastNew;
    private Integer total;

    public String getPool() {
        return pool;
    }

    public void setPool(String pool) {
        this.pool = pool;
    }

    public Integer getLastNew() {
        return lastNew;
    }

    public void setLastNew(Integer lastNew) {
        this.lastNew = lastNew;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "PoolInfo{" +
                "lastNew=" + lastNew +
                ", total=" + total +
                '}';
    }
}
