package com.briup.bean.gacha;

import io.swagger.models.auth.In;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_data")
public class UserData {
    @Id
    private String poolUid;
    private String beginTime;
    private String endTime;
    private String uid;
    private String pool;
    private Integer total;
    private Double avgR6;
    private Double avgR5;
    private Double avgR4;
    private Double avgR3;
    private Integer LastNew;



    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getPoolUid() {
        return poolUid;
    }

    public void setPoolUid(String poolUid) {
        this.poolUid = poolUid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPool() {
        return pool;
    }

    public void setPool(String pool) {
        this.pool = pool;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Double getAvgR6() {
        return avgR6;
    }

    public void setAvgR6(Double avgR6) {
        this.avgR6 = avgR6;
    }

    public Double getAvgR5() {
        return avgR5;
    }

    public void setAvgR5(Double avgR5) {
        this.avgR5 = avgR5;
    }

    public Double getAvgR4() {
        return avgR4;
    }

    public void setAvgR4(Double avgR4) {
        this.avgR4 = avgR4;
    }

    public Double getAvgR3() {
        return avgR3;
    }

    public void setAvgR3(Double avgR3) {
        this.avgR3 = avgR3;
    }

    public Integer getLastNew() {
        return LastNew;
    }

    public void setLastNew(Integer lastNew) {
        LastNew = lastNew;
    }


    @Override
    public String toString() {
        return "UserData{" +
                "poolUid='" + poolUid + '\'' +
                ", beginTime='" + beginTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", uid='" + uid + '\'' +
                ", pool='" + pool + '\'' +
                ", total=" + total +
                ", avgR6=" + avgR6 +
                ", avgR5=" + avgR5 +
                ", avgR4=" + avgR4 +
                ", avgR3=" + avgR3 +
                ", LastNew=" + LastNew +
                '}';
    }
}
