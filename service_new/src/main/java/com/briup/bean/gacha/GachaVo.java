package com.briup.bean.gacha;

import java.util.List;

public class GachaVo {

    private Integer total;
    private String beginAndEndTime;
    private Double avgR6;
    private Double avgR5;
    private Double avgR4;
    private Double avgR3;
    private List<PoolInfo> poolInfo;
    private List<GachaResultVo> gachaResultVo;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getBeginAndEndTime() {
        return beginAndEndTime;
    }

    public void setBeginAndEndTime(String beginAndEndTime) {
        this.beginAndEndTime = beginAndEndTime;
    }

    public Double getAvgR6() {
        return avgR6;
    }

    public void setAvgR6(Double avgR6) {
        this.avgR6 = avgR6;
    }

    public Double getAvgR5() {
        return avgR5;
    }

    public void setAvgR5(Double avgR5) {
        this.avgR5 = avgR5;
    }

    public Double getAvgR4() {
        return avgR4;
    }

    public void setAvgR4(Double avgR4) {
        this.avgR4 = avgR4;
    }

    public Double getAvgR3() {
        return avgR3;
    }

    public void setAvgR3(Double avgR3) {
        this.avgR3 = avgR3;
    }

    public List<PoolInfo> getPoolInfo() {
        return poolInfo;
    }

    public void setPoolInfo(List<PoolInfo> poolInfo) {
        this.poolInfo = poolInfo;
    }

    public List<GachaResultVo> getGachaResultVo() {
        return gachaResultVo;
    }

    public void setGachaResultVo(List<GachaResultVo> gachaResultVo) {
        this.gachaResultVo = gachaResultVo;
    }

    @Override
    public String toString() {
        return "GachaVo{" +
                "total=" + total +
                ", beginAndEndTime='" + beginAndEndTime + '\'' +
                ", avgR6=" + avgR6 +
                ", avgR5=" + avgR5 +
                ", avgR4=" + avgR4 +
                ", avgR3=" + avgR3 +
                ", poolInfo=" + poolInfo +
                ", gachaResultVo=" + gachaResultVo +
                '}';
    }
}
