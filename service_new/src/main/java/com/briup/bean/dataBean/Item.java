package com.briup.bean.dataBean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="item")
public class Item implements Serializable{
	private static final long serialVersionUID = 1L;



	@Id
	private String itemId;
	
	private String itemName;
	
	private String itemEnName;
	
	private Double itemValue;
	
	private String type;

	
	
	public Item() {
		super();
	}



	public String getItemId() {
		return itemId;
	}



	public void setItemId(String itemId) {
		this.itemId = itemId;
	}



	public String getItemName() {
		return itemName;
	}



	public void setItemName(String itemName) {
		this.itemName = itemName;
	}



	public String getItemEnName() {
		return itemEnName;
	}



	public void setItemEnName(String itemEnName) {
		this.itemEnName = itemEnName;
	}



	public Double getItemValue() {
		return itemValue;
	}



	public void setItemValue(Double itemValue) {
		this.itemValue = itemValue;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}






	public Item(String itemId, String itemName, String itemEnName, Double itemValue, String type) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.itemEnName = itemEnName;
		this.itemValue = itemValue;
		this.type = type;
	}



	@Override
	public String toString() {
		return "Item [itemId=" + itemId + ", itemName=" + itemName + ", itemEnName=" + itemEnName + ", itemValue="
				+ itemValue + ", type=" + type + "]";
	}




}
