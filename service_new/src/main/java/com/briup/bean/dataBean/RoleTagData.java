package com.briup.bean.dataBean;

import com.alibaba.excel.annotation.ExcelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "recruitment_info")
public class RoleTagData implements Serializable {
    private static final long serialVersionUID = 1L;


    @Id
    @ExcelProperty("序号")
    private Integer id;
    @ExcelProperty("干员")
    private String roleName;
    @ExcelProperty("可否招募")
    private Integer type;
    @ExcelProperty("星级")
    private Integer rarity;
    @ExcelProperty("站位")
    private String tag1;
    @ExcelProperty("词条1")
    private String tag2;
    @ExcelProperty("词条2")
    private String tag3;
    @ExcelProperty("词条3")
    private String tag4;
    @ExcelProperty("词条4")
    private String tag5;
    @ExcelProperty("词条5")
    private String tag6;

    @ExcelProperty("词条值")
    private String tagValue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRarity() {
        return rarity;
    }

    public void setRarity(Integer rarity) {
        this.rarity = rarity;
    }

    public String getTag1() {
        return tag1;
    }

    public void setTag1(String tag1) {
        this.tag1 = tag1;
    }

    public String getTag2() {
        return tag2;
    }

    public void setTag2(String tag2) {
        this.tag2 = tag2;
    }

    public String getTag3() {
        return tag3;
    }

    public void setTag3(String tag3) {
        this.tag3 = tag3;
    }

    public String getTag4() {
        return tag4;
    }

    public void setTag4(String tag4) {
        this.tag4 = tag4;
    }

    public String getTag5() {
        return tag5;
    }

    public void setTag5(String tag5) {
        this.tag5 = tag5;
    }

    public String getTag6() {
        return tag6;
    }

    public void setTag6(String tag6) {
        this.tag6 = tag6;
    }



    public String getTagValue() {
        return tagValue;
    }

    public void setTagValue(String tagValue) {
        this.tagValue = tagValue;
    }

    public RoleTagData() {
    }

    @Override
    public String toString() {
        return "RoleTagData{" +
                "id=" + id +
                ", roleName='" + roleName + '\'' +
                ", type=" + type +
                ", rarity=" + rarity +
                ", tag1='" + tag1 + '\'' +
                ", tag2='" + tag2 + '\'' +
                ", tag3='" + tag3 + '\'' +
                ", tag4='" + tag4 + '\'' +
                ", tag5='" + tag5 + '\'' +
                ", tag6='" + tag6 + '\'' +
                ", tagValue='" + tagValue + '\'' +
                '}';
    }
}
