package com.briup.bean.dataBean;

import com.alibaba.excel.annotation.ExcelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="logistical_skills_data")
public class LogisticalSkillsData implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @ExcelProperty("序号")
    private Integer id;
    @ExcelProperty("角色名称")
    private String roleName;
    @ExcelProperty("进化等级")
    private String evoLevel;
    @ExcelProperty("设施名称")
    private String facilityName;
    @ExcelProperty("技能名称")
    private String skillName;
    @ExcelProperty("技能颜色")
    private String colorType;
    @ExcelProperty("技能描述")
    private String skillDescription;
    @ExcelProperty("筛选条件")
    private String tag;
    @ExcelProperty("副条件")
    private String tagSec;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getEvoLevel() {
        return evoLevel;
    }

    public void setEvoLevel(String evoLevel) {
        this.evoLevel = evoLevel;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    public String getColorType() {
        return colorType;
    }

    public void setColorType(String colorType) {
        this.colorType = colorType;
    }

    public String getSkillDescription() {
        return skillDescription;
    }

    public void setSkillDescription(String skillDescription) {
        this.skillDescription = skillDescription;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTagSec() {
        return tagSec;
    }

    public void setTagSec(String tagSec) {
        this.tagSec = tagSec;
    }

    public LogisticalSkillsData() {
    }

    @Override
    public String toString() {
        return "LogisticalSkillsData{" +
                "id=" + id +
                ", roleName='" + roleName + '\'' +
                ", evoLevel='" + evoLevel + '\'' +
                ", facilityName='" + facilityName + '\'' +
                ", skillName='" + skillName + '\'' +
                ", colorType='" + colorType + '\'' +
                ", skillDescription='" + skillDescription + '\'' +
                ", tag='" + tag + '\'' +
                ", tagSec='" + tagSec + '\'' +
                '}';
    }
}
