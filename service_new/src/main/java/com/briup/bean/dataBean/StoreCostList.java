package com.briup.bean.dataBean;

import com.alibaba.excel.annotation.ExcelProperty;

public class StoreCostList {

    @ExcelProperty("素材名称")
    private  String itemName;

    @ExcelProperty("价格")
    private  String cost;

    @ExcelProperty("类型")
    private String type;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public StoreCostList() {
    }

    @Override
    public String toString() {
        return "StoreCostList{" +
                "itemName='" + itemName + '\'' +
                ", cost='" + cost + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
