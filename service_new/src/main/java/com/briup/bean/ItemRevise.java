package com.briup.bean;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "item_revise")
public class ItemRevise {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private String itemId;

    private String itemName;

    private String itemEnName;

    private Double itemValue;

    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }


    public void setItemName(String itemName) {
        this.itemName = itemName;
    }


    public String getItemEnName() {
        return itemEnName;
    }


    public void setItemEnName(String itemEnName) {
        this.itemEnName = itemEnName;
    }


    public Double getItemValue() {
        return itemValue;
    }


    public void setItemValue(Double itemValue) {
        this.itemValue = itemValue;
    }


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public ItemRevise() {
        super();
    }

    public ItemRevise(Long id,String itemId, String itemName, String itemEnName, Double itemValue, String type) {
        super();
        this.id = id;
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemEnName = itemEnName;
        this.itemValue = itemValue;
        this.type = type;
    }


    @Override
    public String toString() {
        return "ItemRevise{" +
                "id=" + id +
                ", itemId='" + itemId + '\'' +
                ", itemName='" + itemName + '\'' +
                ", itemEnName='" + itemEnName + '\'' +
                ", itemValue=" + itemValue +
                ", type='" + type + '\'' +
                '}';
    }
}
