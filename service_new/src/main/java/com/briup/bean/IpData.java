package com.briup.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ip_data")
public class IpData {

    @Id
    private String id;
    private String ip;
    private String visitTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(String visitTime) {
        this.visitTime = visitTime;
    }

    @Override
    public String toString() {
        return "IpData{" +
                "id='" + id + '\'' +
                ", ip='" + ip + '\'' +
                ", visitTime='" + visitTime + '\'' +
                '}';
    }
}
