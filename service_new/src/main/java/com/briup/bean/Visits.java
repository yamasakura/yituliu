package com.briup.bean;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="visits")
public class Visits implements Serializable {


    @Id
    private String visitsDay;

    private Integer visits;

    public String getVisitsDay() {
        return visitsDay;
    }

    public void setVisitsDay(String day) {
        this.visitsDay = day;
    }

    public Integer getVisits() {
        return visits;
    }

    public void setVisits(Integer visits) {
        this.visits = visits;
    }

    public Visits(String visitsDay, Integer visits) {
        super();
        this.visitsDay = visitsDay;
        this.visits = visits;
    }

    public Visits() {
        super();
    }

    @Override
    public String toString() {
        return "Visits [day=" + visitsDay + ", visits=" + visits + "]";
    }



}
