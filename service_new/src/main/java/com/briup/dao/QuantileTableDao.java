package com.briup.dao;

import com.briup.bean.dataBean.QuantileTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuantileTableDao extends JpaRepository<QuantileTable, Long> {
}
