package com.briup.dao;


import com.briup.bean.dataBean.RoleTagData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleTagDataDao extends JpaRepository<RoleTagData, Long> {

    List<RoleTagData> findByTypeAndRarityBetween(Integer type,Integer rarity1,Integer rarity2);
}
