package com.briup.dao;


import com.briup.bean.gacha.UserData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDataDao extends JpaRepository<UserData, Long> {

        List<UserData> findAllByUid(String uid);

}
