package com.briup.dao;


import com.briup.bean.dataBean.LogisticalSkillsData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LogisticalSkillsDataDao extends JpaRepository<LogisticalSkillsData, Long> {

    List<LogisticalSkillsData> findByFacilityName(String facilityName);

}
