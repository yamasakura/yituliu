package com.briup.dao;

import com.briup.bean.IpData;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IpDataDao extends JpaRepository<IpData, Long> {

    IpData findIpDataByIpAndVisitTime(String ip,String visitTime);

}
