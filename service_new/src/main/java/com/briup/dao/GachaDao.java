package com.briup.dao;



import com.briup.bean.gacha.GachaData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface GachaDao extends JpaRepository<GachaData, Long> {

      List<GachaData> findAllByGachaIdLikeOrderByGachaIdAsc(String gachaId);

    @Transactional
    @Modifying
    @Query(value = "SELECT * FROM gacha_data WHERE gacha_id like ?% LIMIT ?,1000",nativeQuery = true)
    List<GachaData> findByGachaIdLimit(String id,Integer pageNum);

//    @Transactional
//    @Modifying
    @Query(value = "SELECT count(gacha_id) FROM gacha_data",nativeQuery = true)
    Integer countSize();
}
