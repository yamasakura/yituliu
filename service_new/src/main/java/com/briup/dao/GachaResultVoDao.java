package com.briup.dao;

import com.briup.bean.ItemRevise;
import com.briup.bean.gacha.GachaResultVo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GachaResultVoDao extends JpaRepository<GachaResultVo, Long> {


    List<GachaResultVo> findByTsUidLikeOrderByTsDesc(String uid);
}
