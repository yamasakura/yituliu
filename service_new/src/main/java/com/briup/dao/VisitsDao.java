package com.briup.dao;


import com.briup.bean.Visits;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VisitsDao extends JpaRepository<Visits, Long> {

    Visits findByVisitsDay(String day);

}
