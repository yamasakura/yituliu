package com.briup.dao;


import com.briup.bean.vo.StageResultVo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface StageResultVoDao extends JpaRepository<StageResultVo, Long> {

    @Transactional
    @Modifying
    @Query(value = "TRUNCATE TABLE stage_result_vo",nativeQuery = true)
    void deleteQuery();

    @Transactional
    @Modifying
    @Query(value = "SELECT *  FROM stage_result_vo WHERE chapter_name = ?1 and main IS not  NULL ORDER BY  code ASC ",nativeQuery = true)
    List<StageResultVo> findByStageId(String stageId);


//    //根据物品名称查询期望理智升序
//    Page<StageResultVo> findByItemNameAndIsShowAndEfficiencyGreaterThanOrderByExpectAsc(String itemName, Integer isShow, Double efficiency, Pageable pageable);
    //根据物品类别查询期望理智升序
    Page<StageResultVo> findByTypeAndIsShowAndEfficiencyGreaterThanOrderByExpectAsc(String itemName,Integer isShow,Double efficiency,Pageable pageable);
    //根据物品类别查询效率升序
    Page<StageResultVo> findByTypeAndIsShowAndEfficiencyGreaterThanOrderByEfficiencyDesc(String main,Integer isShow,Double efficiency,Pageable pageable);
    //查询所有关卡信息
    List<StageResultVo> findByTypeNotNullOrderByEfficiencyDesc();

}
