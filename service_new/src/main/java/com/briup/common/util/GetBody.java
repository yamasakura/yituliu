package com.briup.common.util;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;


public class GetBody {

	public static String getPageContent(String url) {

		CloseableHttpClient closeableHttpClient = HttpClientBuilder.create().build();
		HttpGet httpGet = new HttpGet(url);
		CloseableHttpResponse response = null;
		String  str = null;
		try {
			response = closeableHttpClient.execute(httpGet);
			HttpEntity msg = response.getEntity();
			System.out.println(response.getStatusLine());//当前响应的状态
//			System.out.println(EntityUtils.toString(msg));//获取服务器响应信息
			str = EntityUtils.toString(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}


		return str;
		
	}
	
	
	
	
}
