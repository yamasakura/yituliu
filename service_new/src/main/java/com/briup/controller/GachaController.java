package com.briup.controller;

import com.briup.bean.gacha.*;
import com.briup.common.util.Result;
import com.briup.service.GachaService;
import com.briup.bean.gacha.GachaVo;
import com.briup.common.util.Result;
import com.briup.service.GachaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "读取官网寻访记录")
@RequestMapping(value = "/gacha")
@CrossOrigin()
public class GachaController {

    @Autowired
    private GachaService gachaService;

    @ApiOperation(value = "输入token查询")
    @GetMapping("/save")
    public Result saveAllGachaInfo(Token token) {

        System.out.println("返回的token：————"+token.getToken());
        Map<String, String> map = gachaService.saveGachaData(token.getToken());


        return Result.success(map);
    }

    @ApiOperation(value = "输入UID返回记录")
    @GetMapping("/find/{userName}")
    public Result findAllGachaInfo(@PathVariable("userName") String userName) {


        GachaVo all = gachaService.findById(userName);

        return Result.success(all);
    }

    @ApiOperation(value = "测试存储量")
    @GetMapping("/save1")
    public Result findAllGachaInfoTest() {


       gachaService.test();

        return Result.success();
    }

    @ApiOperation(value = "测试存储量")
    @GetMapping("/save2/{uid}")
    public Result findAllGachaData(@PathVariable("uid") String uid) {

        HashMap<String, Integer> allData = gachaService.findAllData(uid);

        return Result.success(allData);
    }

}
