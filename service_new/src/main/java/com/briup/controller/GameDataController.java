package com.briup.controller;


import com.briup.bean.dataBean.Item;
import com.briup.common.util.Result;
import com.briup.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@Api(tags = "游戏数据更新")
@CrossOrigin
@RequestMapping(value = "/item", produces = "application/json;charset=UTF-8")
public class GameDataController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private StageService stageService;

    @Autowired
    private RoleTagDataService roleTagDataService;

    @Autowired
    private  LogisticalSkillsDataService logisticalSkillsDataService;





    @ApiOperation(value = "基准价值")
    @GetMapping("/item")
    public Result save() {

        itemService.item();
        List<Item> all = itemService.findAll();
        return Result.success( all);
    }


    @ApiOperation(value = "关卡信息导入")
    @PostMapping("/importStageData")
    public Result importStageData(MultipartFile file) {

        stageService.importStageData(file);

        return Result.success("成功");
    }

    @ApiOperation(value = "关卡信息导出")
    @GetMapping("/exportStageData")
    public void exportStageData(HttpServletResponse response) {

        stageService.exportStageData(response);

    }



    @ApiOperation(value = "后勤技能数据导入")
    @PostMapping("/importLogisticalSkillsData")
    public Result importLogisticalSkillsData(MultipartFile file) {

        logisticalSkillsDataService.save(file);

        return Result.success("成功");
    }

    @ApiOperation(value = "后勤技能数据导出")
    @GetMapping("/exportLogisticalSkillsData")
    public void exportLogisticalSkillsData(HttpServletResponse response) {

        logisticalSkillsDataService.exportLogisticalSkillsData(response);

    }

    @ApiOperation("公招数据导入")
    @PostMapping("/importRecruitmentInfo")
    public Result importRecruitmentInfo(MultipartFile file) {

        roleTagDataService.saveAll(file);

        return Result.success();
    }

    @ApiOperation("公招数据导出")
    @GetMapping("/exportRecruitmentInfo")
    public void exportRecruitmentInfo(HttpServletResponse response) {

        roleTagDataService.exportData(response);
    }


    @ApiOperation("导出物品价值表")
    @GetMapping("/exportItemData")
    public void exportItemData(HttpServletResponse response) {
        itemService.exportItemData(response);
    }






}
