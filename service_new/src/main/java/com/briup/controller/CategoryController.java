package com.briup.controller;


import com.briup.bean.*;
import com.briup.bean.dataBean.Stage;
import com.briup.bean.vo.StageResultVo;
import com.briup.common.util.Result;
import com.briup.common.util.ResultCode;
import com.briup.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


@RestController
@Api(tags = "获取数据")
@RequestMapping(value = "/category")
@CrossOrigin(maxAge = 86400)
public class CategoryController {



    @Autowired
    private CategoryService categoryService;

    @Autowired
    private StoreCostPerService storeCostPerService;

    @Autowired
    private ItemService itemService;



    @ApiOperation("根据主产物，按照效率降序排列（无限龙门币）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ends", value = "0为常驻副本", required = true, paramType = "path", defaultValue = "1"),
            @ApiImplicitParam(name = "efficiency", value = "效率最小值", required = true, paramType = "path", defaultValue = "1.0")})
    @GetMapping("/findAll/LMB/{ends}/{efficiency}")

    public Result findByTypeAndEndsOrderByEfficiencyDescLmb (@PathVariable("ends") Integer ends,
                                                            @PathVariable("efficiency") Double efficiency) {

        List<Object> byEffAndIsShow = categoryService.findByEffAndIsShow(ends, efficiency);


        return Result.success(byEffAndIsShow);
    }


    @ApiOperation(value = "按照商店类型获取")
    @GetMapping("/findStore/{type}")
    public Result findStoreType(@PathVariable("type") String type) {

        List<StoreCostPer> all = storeCostPerService.findAll(type);

        return Result.success(all);
    }

    @ApiOperation(value = "更新常驻商店性价比")

    @PostMapping("/store/update")
    public Result updateStore(MultipartFile file) {
        storeCostPerService.update(file);
        return Result.success();
    }

    @ApiOperation("获取常驻商店性价比")
    @GetMapping("/findAll/store")
    public Result findAll() {
        List<Object> storeAllData = new ArrayList<>();
        storeAllData.add(storeCostPerService.findAll("green"));
        storeAllData.add(storeCostPerService.findAll("orange"));
        storeAllData.add(storeCostPerService.findAll("purple"));
        storeAllData.add(storeCostPerService.findAll("yellow"));
        storeAllData.add(storeCostPerService.findAll("grey"));
        return Result.success(storeAllData);
    }


    @ApiOperation("获取物品价值")
    @GetMapping("/findAll/item")
    public Result findAllItem() {

        List<ItemRevise> all =itemService.findAllItemRevise();

        return Result.success(all);
    }


    @ApiOperation("获取全部关卡效率")
    @GetMapping("/findAllStageEff")
    public Result findAllStageEff() {

        List<StageResultVo> all = categoryService.findByMainNotNull();

        return Result.success(all);
    }



    @GetMapping("/selectVisits/{begin}/{end}")
    public Result selectVisits(@PathVariable("begin") String begin,
                               @PathVariable("end") String end ,HttpServletRequest request)  {

        List<Object> list = categoryService.selectVisits(begin, end,request);

        if (list.size() > 0) {
            return Result.success(list);
        } else {
            return Result.success(ResultCode.DATA_NONE);
        }
    }

    @ApiOperation("更新关卡信息")
    @GetMapping("/updateStageInfo/{isShow}/{stageId}")
    public Result updateStageInfo(@PathVariable("isShow") Integer isShow, @PathVariable("stageId") String stageId) {
                categoryService.updateStageInfo(isShow,stageId);
        return Result.success();
    }

    @ApiOperation("获取关卡信息")
    @GetMapping("/findAllStage")
    public Result findStageInfo() {
      List<Stage> stageList =   categoryService.findAllStageInfo();
        return Result.success(stageList);
    }

    @ApiOperation("获取往期活动地图效率")
    @GetMapping("/findAll/StageId/{actName}/{stageName}")
    public Result findAllByStageId(@PathVariable("actName")List<String> actName
                                  ,@PathVariable("stageName")List<String> stageName) {


        List<Object> all = new ArrayList<>();
        for (int i = 0; i < actName.size(); i++) {
            //查出活动关卡

            List<StageResultVo>  list = new ArrayList<>();
            List<StageResultVo>  listCopy = new ArrayList<>();

            if("多索雷斯假日".equals(actName.get(i))) {
                List<StageResultVo> penguinCopies = stageData();
                listCopy = penguinCopies;

            }else {
//                list = categoryService.findByStageId(stageName.get(i));
                listCopy = categoryService.findByStageId(stageName.get(i));
            }

            for(int k=0;k<listCopy.size();k++){
                    if("异铁".equals(listCopy.get(k).getMain())||"酮凝集".equals(listCopy.get(k).getMain())
                    ||"糖".equals(listCopy.get(k).getMain()) ||"装置".equals(listCopy.get(k).getMain())
                    ||"聚酸酯".equals(listCopy.get(k).getMain()) ||"固源岩".equals(listCopy.get(k).getMain())){
                    }else {
                        list.add(listCopy.get(k));
                    }
            }

            for(int j=0;j<list.size();j++){
                //查出主线关卡算相对效率


                Page<StageResultVo> dataSelectByTypeByEfficiencyDesc = categoryService.findDataSelectByTypeByEfficiencyDesc(
                        list.get(j).getMain(), 1, 0.5, 0, 3);

                DecimalFormat dfbfb = new DecimalFormat("0.0");
                double percentage = 0.0;
                if (dataSelectByTypeByEfficiencyDesc.getContent().get(0).getIsUseValue()==1){
                     percentage = list.get(j).getEfficiency()/dataSelectByTypeByEfficiencyDesc.getContent().get(0).getEfficiency();
                }else {
                     percentage = list.get(j).getEfficiency()/dataSelectByTypeByEfficiencyDesc.getContent().get(1).getEfficiency();
                }
                list.get(j).setPercentage(Double.valueOf(dfbfb.format( percentage* 100)));
            }

            if(list.size()>0){
                list.get(0).setActivityName(actName.get(i));

            }


            all.add(list);
        }
        return Result.success(all);
    }



    //设置特殊类型副本数值
    private  List<StageResultVo> stageData(){

        DecimalFormat dfbfb = new DecimalFormat("0.00");

        StageResultVo stageData =  new StageResultVo();
        stageData.setStageName("DH-*");
        stageData.setMain("异铁组");
        stageData.setSecondaryId("1");
        stageData.setItemId("30043");
        stageData.setProbability((18*1.95)/50);
        stageData.setExpect(Double.valueOf(dfbfb.format( 50/1.95)));
        stageData.setChapterName("多索雷斯假日");
        stageData.setEfficiency(Double.valueOf(dfbfb.format(
                (((18*1.95)/50)*27.734+18*0.06)/18*1.25 )));

        StageResultVo stageData1 =  new StageResultVo();
        stageData1.setStageName("DH-*");
        stageData1.setMain("轻锰矿");
        stageData1.setSecondaryId("1");
        stageData1.setItemId("30083");
        stageData1.setProbability((18*1.95)/50);
        stageData1.setExpect(Double.valueOf(dfbfb.format( 50/1.95)));
        stageData1.setChapterName("多索雷斯假日");
        stageData1.setEfficiency(Double.valueOf(dfbfb.format(
                (((18*1.95)/50)*27.174+18*0.06)/18*1.25 )));

        StageResultVo stageData2 =  new StageResultVo();
        stageData2.setStageName("DH-*");
        stageData2.setMain("固源岩组");
        stageData2.setSecondaryId("1");
        stageData2.setItemId("30013");
        stageData2.setProbability((18*1.95)/35);
        stageData2.setExpect(Double.valueOf(dfbfb.format(35/1.95)));
        stageData2.setChapterName("多索雷斯假日");
        stageData2.setEfficiency(Double.valueOf(dfbfb.format(
                (((18*1.95)/35)*17.756+18*0.06)/18*1.25 )));

        List<StageResultVo> list = new ArrayList<>();
        list.add(stageData);
        list.add(stageData1);
        list.add(stageData2);

        return    list;
    }




}
