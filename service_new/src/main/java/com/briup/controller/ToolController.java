package com.briup.controller;


import com.briup.bean.dataBean.LogisticalSkillsData;

import com.briup.bean.dataBean.RoleTagData;
import com.briup.bean.vo.RecResultVo;
import com.briup.common.util.Result;
import com.briup.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@Api(tags = "工具接口")
@RequestMapping(value = "/tool")
@CrossOrigin(maxAge = 86400)
public class ToolController {



    @Autowired
    private RoleTagDataService roleTagDataService;

    @Autowired
    private LogisticalSkillsDataService logisticalSkillsDataService;



//    @ApiOperation("读取数据（后勤技能）")
//    @GetMapping("/findAll/LogisticalSkillsData/{name}")
//    public Result findAllLogisticalSkillsData(@PathVariable("name") String name) {
//        List<LogisticalSkillsData> allData = logisticalSkillsDataService.findAllByFacilityName(name);return Result.success(allData);
//    }

    @ApiOperation("读取数据Json（后勤技能）")
    @GetMapping("/findAll/LogisticalSkillsData")
    public Result findAllLogisticalSkillsDataJson() {
        List<LogisticalSkillsData> allData = logisticalSkillsDataService.findAll();
        return Result.success(allData);
    }


    @ApiOperation("读取数据（公招）")
    @GetMapping("/findAll/recruitmentInfo/{type}/{tags}/{rarityMax}")
    public Result findAllRecruitmentInfoDataByType(@PathVariable("type") Integer type,
                                                   @PathVariable("tags") String[] tags,
                                                   @PathVariable("rarityMax") Integer rarityMax) {
        List<RecResultVo> resultListVo = roleTagDataService.findAllByTypeAndRarityNew(type, tags, rarityMax);

        return Result.success(resultListVo);
    }

//    @ApiOperation("读取基础数据（公招）")
//    @GetMapping("/findAll/recruitmentInfo")
//    public Result findAllRecruitmentInfoData() {
//        List<RoleTagData> all = roleTagDataService.findAll();return Result.success(all);
//    }

}
