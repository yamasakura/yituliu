package com.briup.controller;

import com.briup.bean.PenguinData;
import com.briup.bean.vo.StageResultVo;
import com.briup.common.util.Result;
import com.briup.service.StageResultVoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@Api(tags = "一图流数据更新")
@CrossOrigin
@RequestMapping(value = "/stage")
public class StageResultController {


    @Autowired
    private StageResultVoService stageResultVoService;



    @ApiOperation("更新关卡数据")
    @GetMapping("/updateNew")
    public Result update() throws Exception {
        double start = System.currentTimeMillis();//记录程序启动时间
          stageResultVoService.stageResult(1);
//          Thread.sleep(5000);
        List<PenguinData> list = stageResultVoService.stageResult(2);

        List<StageResultVo> stageResultVoList = new ArrayList<>();
        for(PenguinData rawData:list ){
            StageResultVo stageResult = new StageResultVo();
            stageResult.setId(rawData.getId());
            stageResult.setStageId(rawData.getStageId());
            stageResult.setStageName(rawData.getStageName());
            stageResult.setCode(rawData.getCode());
            stageResult.setChapterName(rawData.getChapterName());
            stageResult.setItemId(rawData.getItemId());
            stageResult.setTimes(rawData.getTimes());
            stageResult.setProbability(rawData.getProbability());
            stageResult.setExpect(rawData.getExpect());
            stageResult.setEfficiency(rawData.getEfficiency());
            stageResult.setMain(rawData.getMain());
            stageResult.setType(rawData.getType());
            stageResult.setSecondary(rawData.getSecondary());
            stageResult.setSecondaryId(rawData.getSecondaryId());
            stageResult.setColor(rawData.getColor());
            stageResult.setUpdateDate(rawData.getUpdateDate());
            stageResult.setSpm(String.valueOf(rawData.getSpm()));
            stageResult.setExtraItem(rawData.getActivityName());
            stageResult.setIsShow(rawData.getIsShow());
            stageResult.setIsUseValue(rawData.getIsValue());
            stageResult.setRequset(rawData.getRequest());
            stageResult.setEfficiencyEx(rawData.getEfficiencyEx());
            stageResult.setConfidence(rawData.getConfidence());
//            System.out.println(stageResult);
            stageResultVoList.add(stageResult);

        }
         stageResultVoService.deleteInBatch();
        stageResultVoService.saveAll(stageResultVoList);

        double end = System.currentTimeMillis();
        System.out.println("用时:---" + (end - start) / 1000 + "s");
        return Result.success(stageResultVoList);
    }


}
