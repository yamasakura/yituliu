package com.briup.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.briup.bean.ItemRevise;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemReviseMapper extends BaseMapper<ItemRevise> {
}
