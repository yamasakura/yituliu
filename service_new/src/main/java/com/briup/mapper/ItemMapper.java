package com.briup.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.briup.bean.dataBean.Item;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemMapper extends BaseMapper<Item> {
}
