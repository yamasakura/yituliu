package com.briup.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.briup.bean.vo.StageResultVo;
import org.springframework.stereotype.Repository;

@Repository
public interface StageResultMapper extends BaseMapper<StageResultVo> {
}
