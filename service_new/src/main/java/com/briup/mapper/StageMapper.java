package com.briup.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.briup.bean.dataBean.Stage;
import org.springframework.stereotype.Repository;

@Repository
public interface StageMapper extends BaseMapper<Stage> {


}
