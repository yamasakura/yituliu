package com.briup.filter;

import com.briup.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin(maxAge = 86400)
public class Filter implements HandlerInterceptor {

    /**
     * 自定义拦截器类
     */
    @Autowired
    private CategoryService categoryService;

        /**
         * 访问控制器方法前执行
         */
        @Override
        public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
                throws Exception {
                if("OPTIONS".equals(request.getMethod())){
//                    categoryService.opVisits();
                }


//                System.out.println(request.getMethod());
//                System.out.println("请求类型"+HttpMethod.OPTIONS.toString()+"访问Url："+request.getRequestURL());
//                System.out.println("请求类型"+HttpMethod.GET.toString()+"访问Url："+request.getRequestURL());
            return true;
        }

        /**
         * 访问控制器方法后执行
         */
        @Override
        public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                               ModelAndView modelAndView) throws Exception {

        }

        /**
         * postHandle方法执行完成后执行，一般用于释放资源
         */
        @Override
        public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
                throws Exception {

        }



}
