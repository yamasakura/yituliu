package com.briup.service;

import com.briup.bean.dataBean.LogisticalSkillsData;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface LogisticalSkillsDataService {


     List<LogisticalSkillsData> findAllByFacilityName(String name);

    void save(MultipartFile file);

    void exportLogisticalSkillsData(HttpServletResponse response);

    List<LogisticalSkillsData> findAll();
}
