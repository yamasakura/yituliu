package com.briup.service.impl;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.briup.bean.dataBean.LogisticalSkillsData;
import com.briup.dao.LogisticalSkillsDataDao;
import com.briup.service.LogisticalSkillsDataService;

import java.net.URLEncoder;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class LogisticalSkillsDataServiceImpl implements LogisticalSkillsDataService {

    @Autowired
    private LogisticalSkillsDataDao logisticalSkillsDataDao;

    @Override
    public List<LogisticalSkillsData> findAllByFacilityName(String name) {

        List<LogisticalSkillsData> allData = logisticalSkillsDataDao.findByFacilityName(name);

        return allData;
    }

    @Override
    public void save(MultipartFile file) {

        List<LogisticalSkillsData> list = new ArrayList<>();
        LogisticalSkillsData headData = new LogisticalSkillsData();


        try {
            EasyExcel.read(file.getInputStream(), LogisticalSkillsData.class, new AnalysisEventListener<LogisticalSkillsData>() {


                @Override
                public void invoke(LogisticalSkillsData logisticalSkillsData, AnalysisContext analysisContext) {

                    list.add(logisticalSkillsData);
                }


                @Override
                public void doAfterAllAnalysed(AnalysisContext analysisContext) {

                }
            }).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }


//        String fileName = "F:\\excel\\03.xlsx";
//        EasyExcel.read(fileName, DataClass.class, new AnalysisEventListener<DataClass>() {
//
//            String roleName = null;
//            int id = 1;
//            @Override
//            public void invoke(DataClass dataClass, AnalysisContext analysisContext) {
//
//                if(dataClass.getDatas().length()<20){
//                    roleName  = dataClass.getDatas();
//                }else {
//                    String[] split = dataClass.getDatas().split("/");
////                    System.out.println(roleName+"---"+split[0]+"---"+split[1]+"---"+split[2]+"---"+split[3]);
//                    LogisticalSkillsData logisticalSkillsData = new LogisticalSkillsData();
//                    logisticalSkillsData.setId(id);
//                    logisticalSkillsData.setRoleName(roleName);
//                    logisticalSkillsData.setEvoLevel(split[0]);
//                    logisticalSkillsData.setFacilityName(split[1]);
//                    if("控制中枢".equals(split[1])){
//                        logisticalSkillsData.setColorType("green2");
//                    }else if("发电站".equals(split[1])){
//                        logisticalSkillsData.setColorType("green2");
//                    }else if("制造站".equals(split[1])){
//                        logisticalSkillsData.setColorType("orange");
//                    }else if("贸易站".equals(split[1])){
//                        logisticalSkillsData.setColorType("blue2");
//                    }else if("加工站".equals(split[1])){
//                        logisticalSkillsData.setColorType("green1");
//                    }else if("训练室".equals(split[1])){
//                        logisticalSkillsData.setColorType("red2");
//                    }else if("宿舍".equals(split[1])){
//                        logisticalSkillsData.setColorType("blue1");
//                    }else if("办公室".equals(split[1])){
//                        logisticalSkillsData.setColorType("grey");
//                    }else if("会客室".equals(split[1])){
//                        logisticalSkillsData.setColorType("red1");
//                    }
//
//                    logisticalSkillsData.setSkillName(split[2]);
//                    logisticalSkillsData.setSkillDescription(split[3]);
//                    id++;
//                    System.out.println(logisticalSkillsData);
//                    list.add(logisticalSkillsData);
//                }
//            }
//
//            @Override
//            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
//
//            }
//        }).sheet().doRead();

        System.out.println(list);
        logisticalSkillsDataDao.saveAll(list);

    }

    @Override
    public void exportLogisticalSkillsData(HttpServletResponse response) {

        List<LogisticalSkillsData> list = logisticalSkillsDataDao.findAll();
        List<LogisticalSkillsData> list1 = new ArrayList<>();

//                String fileName1 = "E:\\EasyExcel\\03.xlsx";
//        EasyExcel.read(fileName1, DataClass.class, new AnalysisEventListener<DataClass>() {
//
//            String roleName = null;
//            int id = 1;
//            @Override
//            public void invoke(DataClass dataClass, AnalysisContext analysisContext) {
//
//                if(dataClass.getDatas().length()<20){
//                    roleName  = dataClass.getDatas();
//                }else {
//                    String[] split = dataClass.getDatas().split("/");
////                    System.out.println(roleName+"---"+split[0]+"---"+split[1]+"---"+split[2]+"---"+split[3]);
//                    LogisticalSkillsData logisticalSkillsData = new LogisticalSkillsData();
//                    logisticalSkillsData.setId(id);
//                    logisticalSkillsData.setRoleName(roleName);
//                    logisticalSkillsData.setEvoLevel(split[0]);
//                    logisticalSkillsData.setFacilityName(split[1]);
//                    if("控制中枢".equals(split[1])){
//                        logisticalSkillsData.setColorType("green2");
//                    }else if("发电站".equals(split[1])){
//                        logisticalSkillsData.setColorType("green2");
//                    }else if("制造站".equals(split[1])){
//                        logisticalSkillsData.setColorType("orange");
//                    }else if("贸易站".equals(split[1])){
//                        logisticalSkillsData.setColorType("blue2");
//                    }else if("加工站".equals(split[1])){
//                        logisticalSkillsData.setColorType("green1");
//                    }else if("训练室".equals(split[1])){
//                        logisticalSkillsData.setColorType("red2");
//                    }else if("宿舍".equals(split[1])){
//                        logisticalSkillsData.setColorType("blue1");
//                    }else if("办公室".equals(split[1])){
//                        logisticalSkillsData.setColorType("grey");
//                    }else if("会客室".equals(split[1])){
//                        logisticalSkillsData.setColorType("red1");
//                    }
//
//                    logisticalSkillsData.setSkillName(split[2]);
//                    logisticalSkillsData.setSkillDescription(split[3]);
//                    id++;
//                    System.out.println(logisticalSkillsData);
//                    list.add(logisticalSkillsData);
//                }
//            }
//
//            @Override
//            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
//
//            }
//        }).sheet().doRead();

//        LogisticalSkillsData[] listStr = new LogisticalSkillsData[list.size()];
//        for (int i = 0; i < list.size(); i++) {
//            listStr[i] = list.get(i);
//        }
//
//        for (int i = 0; i < listStr.length; i+=2) {
//            for (int j = 1; j < listStr.length; j+=2) {
//                if(listStr[i].getRoleName().equals(listStr[j].getRoleName())){
//                    LogisticalSkillsData t = new LogisticalSkillsData();
//                    System.out.println("第"+i+"条数据");
//                      t = listStr[i+1];
//                    listStr[i+1] = listStr[j];
//                    listStr[j] = t;
//                }
//            }
//        }
//        List<LogisticalSkillsData> listVo = new ArrayList<>();
//
//        for (int i = 0; i < listStr.length; i++) {
//            listVo.add(listStr[i]);
//        }

        try {
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
// 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("logisticalSkillsData", "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");


            EasyExcel.write(response.getOutputStream(), LogisticalSkillsData.class).sheet("Sheet1").doWrite(list);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<LogisticalSkillsData> findAll() {

        List<LogisticalSkillsData> list = logisticalSkillsDataDao.findAll();
        return  list;
    }
}
