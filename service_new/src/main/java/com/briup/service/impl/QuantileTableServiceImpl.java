package com.briup.service.impl;

import com.briup.bean.dataBean.QuantileTable;
import com.briup.dao.QuantileTableDao;
import com.briup.service.QuantileTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuantileTableServiceImpl implements QuantileTableService {

    @Autowired
    private QuantileTableDao quantileTableDao;

    @Override
    public List<QuantileTable> findAll() {
        List<QuantileTable> all = quantileTableDao.findAll();
        return all;
    }
}
