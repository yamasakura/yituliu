package com.briup.service.impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.briup.bean.ItemRevise;
import com.briup.bean.PenguinData;
import com.briup.bean.dataBean.Item;
import com.briup.bean.dataBean.PenguinClass;
import com.briup.bean.dataBean.QuantileTable;
import com.briup.bean.dataBean.Stage;
import com.briup.bean.vo.StageResultVo;
import com.briup.bean.vo.StageVo;
import com.briup.common.exception.ServiceException;
import com.briup.common.util.GetBody;
import com.briup.common.util.GetUtils;
import com.briup.common.util.ResultCode;
import com.briup.dao.StageResultVoDao;
import com.briup.mapper.ItemMapper;
import com.briup.mapper.ItemReviseMapper;
import com.briup.mapper.StageMapper;
import com.briup.service.ItemService;
import com.briup.service.QuantileTableService;
import com.briup.service.StageService;
import com.briup.service.StageResultVoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.util.Comparator.comparing;

@Service
public class StageResultVoServiceImpl implements StageResultVoService {

    @Autowired
    private StageResultVoDao stageResultVoDao;
    @Autowired
    private StageService stageService;

    @Autowired
    private ItemService itemService;

    @Autowired
    private QuantileTableService quantileTableService;

    @Override
    public void saveAll(List<StageResultVo> stageResultVo) {
        if (stageResultVo != null) {
            stageResultVoDao.saveAll(stageResultVo);
        } else {
            throw new ServiceException(ResultCode.PARAM_IS_BLANK);
        }
    }


    @Override
    public void deleteInBatch() {
        stageResultVoDao.deleteQuery();
    }


    @Override
    public List<PenguinData> stageResult(Integer num) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
        String updateTime = simpleDateFormat.format(new Date());
        System.out.println("更新时间是————" + updateTime);
        DecimalFormat DF3 = new DecimalFormat("0.000");
        DecimalFormat DF2 = new DecimalFormat("0.0");

        List<StageVo> stageInfoList = stageService.findAllEx();
        List<Item> itemList = itemService.findAll();
        List<ItemRevise> itemReviseList = itemService.findAllItemRevise();
        List<QuantileTable> quantileTableList = quantileTableService.findAll();

        String url = "https://penguin-stats.io/PenguinStats/api/v2/result/matrix?show_closed_zones=true";
        String con = GetUtils.GetBody(url);
//        String jsonFile = readJsonFile(StageResultVoServiceImpl.class.getClassLoader().getResource("matrix.json").getPath());
        JSONObject obj = JSONObject.parseObject(con);

        List<PenguinClass> penguinDatalist = JSONObject.parseArray(JSON.toJSONString(obj.get("matrix")), PenguinClass.class);
        List<PenguinData> rawDataList = new ArrayList<>();

        double sum = 0.0;
        double sumEx = 0.0;
        int rawDataListCode = 0;
        double sectionValue = 0.0;
        String  confidenceInterval = "";
        String exItem = "1";
        for (int i = 0; i < penguinDatalist.size(); i++) {
            if (i > 0) {
                if (!penguinDatalist.get(i).getStageId().equals(penguinDatalist.get(i - 1).getStageId())) {
                    sum = 0.0;
                    sumEx = 0.0;
                    sectionValue = 0.0;
                     confidenceInterval = "";
                    rawDataListCode = rawDataList.size();
                    exItem = "1";
                }
            }


            int penguinDataTimes = Integer.parseInt(penguinDatalist.get(i).getTimes());
            String penguinDataStageId = penguinDatalist.get(i).getStageId();
            int penguinDataQuantity = Integer.parseInt(penguinDatalist.get(i).getQuantity());
            if (penguinDataQuantity < 1) {
//				System.out.println("第" + i + "个循环跳出——因为掉落次数为零" + penguinDataStageId);
                continue;
            }

            if (penguinDataTimes < 300) {
//                System.out.println("第" + i + "个循环跳出——因为样本太小" + penguinDataStageId);
                continue;
            }

            String penguinDataItemId = penguinDatalist.get(i).getItemId();
            int stageInfoListCode = 0;
            int itemListCode = 0;
            int itemReviseListCode = 0;
            double reason = 0.0;
            double probability = 0.0;
            double request = 0.0;
            double expect = 0.0;
            double efficiency = 0.0;
            double reasonEx = 0.0;
            double requestEx = 0.0;
            double efficiencyEx = 0.0;

            int stageNoNull = 0;
            int itemNoNull = 0;
            int itemReviseNoNull = 0;

            probability = (double) penguinDataQuantity / penguinDataTimes;


            for (int j = 0; j < stageInfoList.size(); j++) {
                if (penguinDataStageId.equals(stageInfoList.get(j).getStageId())) {
                    stageInfoListCode = j;
                    stageNoNull = 1;
                }
            }

            if (stageNoNull == 0) {
//                System.out.println("第" + i + "个循环跳出——因为没有关卡" + penguinDataStageId);
                continue;
            }

            for (int k = 0; k < itemList.size(); k++) {
                if (penguinDataItemId.equals(itemList.get(k).getItemId())) {
                    itemListCode = k;//材料序号
                    itemNoNull = 1;//非空标记
                }
            }


            for (int k = 0; k < itemReviseList.size(); k++) {

                if (penguinDataItemId.equals(itemReviseList.get(k).getItemId())) {
//                    System.out.println(penguinDataItemId + "----" + itemReviseList.get(k).getItemId());
                    itemReviseListCode = k;//材料序号
                    itemReviseNoNull = 1;//非空标记
                }
            }

            if (itemNoNull == 0) {
//                System.out.println("第" + i + "个循环跳出——因为没有材料" + penguinDataStageId);
                continue;
            }

            if (itemReviseNoNull == 0) {
//				System.out.println("第" + i + "个循环跳出——因为没有材料" + penguinDataStageId);
                continue;
            }

//            System.out.println("第" + i + "个循环——" + penguinDataStageId + "————" + stageInfoList.get(stageInfoListCode).getStageId());

            if (num == 2) {
                if ("ap_supply_lt_010".equals(itemReviseList.get(itemReviseListCode).getItemId())) {
                    stageInfoList.get(stageInfoListCode).setReasonEx(stageInfoList.get(stageInfoListCode).getReason() - probability * 10);
                    exItem = "ap_supply_lt_010";
//                    System.out.println("第" + i + "个循环——有小样" + "————" + stageInfoList.get(stageInfoListCode).getStageName());
                }

                if ("randomMaterial_6".equals(itemReviseList.get(itemReviseListCode).getItemId())) {
                    requestEx = itemReviseList.get(itemReviseListCode).getItemValue() * probability;//单次结果
//                    System.out.println("第" + i + "个循环——有物资箱" + "————" + stageInfoList.get(stageInfoListCode).getStageName());
                }

                int quantileTableListCode = 1;

                if (!"1".equals(stageInfoList.get(stageInfoListCode).getSecondary())) {
                    if (stageInfoList.get(stageInfoListCode).getSecondary().equals(itemReviseList.get(itemReviseListCode).getItemName())) {
                        sectionValue = 0.01 / Math.sqrt(1 * probability * (1 - probability) / (penguinDataTimes - 1));
                        for (int j = 1; j < quantileTableList.size(); j++) {
                            if (sectionValue < quantileTableList.get(j).getValue()) { quantileTableListCode = j;break; }
                        }
                          if (sectionValue < 3.09023) {
                            confidenceInterval = DF2.format((quantileTableList.get(quantileTableListCode - 1).getSection()*2-1) * 100) + "%";
                        } else {
                            confidenceInterval = "99.9+%";
                        }
                    }

                } else {
                    if (stageInfoList.get(stageInfoListCode).getMain().equals(itemReviseList.get(itemReviseListCode).getItemName())) {
                        sectionValue = 0.03 / Math.sqrt(1 * probability * (1 - probability) / (penguinDataTimes));
                        for (int j = 1; j < quantileTableList.size(); j++) {
                            if (sectionValue < quantileTableList.get(j).getValue()) { quantileTableListCode = j;break; }
                        }
                       if (sectionValue < 3.09023) {
                            confidenceInterval = DF2.format(quantileTableList.get(quantileTableListCode - 1).getSection() * 100) + "%";
                        } else {
                            confidenceInterval = "99.9+%";
                        }
                    }
                }
            }
            reason = stageInfoList.get(stageInfoListCode).getReason() * 1;
            reasonEx = stageInfoList.get(stageInfoListCode).getReasonEx() * 1;


//            System.out.println(stageInfoList.get(stageInfoListCode).getStageName() + "的理智消耗是：" + reason);
//            System.out.println(stageInfoList.get(stageInfoListCode).getStageName() + "的理智Ex消耗是：" + reasonEx);

            if (num == 1) {
                request = itemList.get(itemListCode).getItemValue() * probability;//单次结果
            } else {
                request = itemReviseList.get(itemReviseListCode).getItemValue() * probability;//单次结果
            }

            if (!stageInfoList.get(stageInfoListCode).getType().equals("半自然溶剂")) {
                if (!stageInfoList.get(stageInfoListCode).getType().equals("化合切削液")) {
                    if ("半自然溶剂".equals(itemReviseList.get(itemReviseListCode).getItemName()) ||
                            "化合切削液".equals(itemReviseList.get(itemReviseListCode).getItemName())) {
//                        System.out.println("第" + i + "个循环——有新材料" + "————" + stageInfoList.get(stageInfoListCode).getStageName());
                        request = request / 3 * 2;

                    }
                }
            }

            expect = reason / probability;
            sum = sum + request - requestEx;
            efficiency = (sum + reason * 0.06) / reason;
            sumEx = sumEx + request;
            efficiencyEx = (sumEx + reasonEx * 0.06) / reasonEx;
//            System.out.println(stageInfoList.get(stageInfoListCode).getStageName()+" ,sum：" + sum + ",reason：" + reason + ",efficiency：" + efficiency);
//            System.out.println(stageInfoList.get(stageInfoListCode).getStageName()+" ,requestEx：" + requestEx +" ,sumEx：" + sumEx + ",reasonEx：" + reasonEx + ",efficiencyEx：" + efficiencyEx);


            PenguinData rawData = new PenguinData();
            rawData.setId((long) i);
            rawData.setStageId(stageInfoList.get(stageInfoListCode).getStageId());
            rawData.setStageName(stageInfoList.get(stageInfoListCode).getStageName());
            rawData.setCode(stageInfoList.get(stageInfoListCode).getChapterCode());
            rawData.setChapterName(stageInfoList.get(stageInfoListCode).getChapterName());
            rawData.setItemId(itemList.get(itemListCode).getItemId());
//			System.out.println("第" + i + "个循环——存入的物品id是" + "————" +itemList.get(itemListCode).getItemId());
            rawData.setTimes(penguinDataTimes);
            rawData.setProbability(probability);
            if ("1-7".equals((stageInfoList.get(stageInfoListCode).getStageName()))) {
                rawData.setItemId("30013");
                rawData.setExpect(expect * 5);

            } else {
                rawData.setExpect(expect);

            }

            rawData.setEfficiency(efficiency);
            if (itemList.get(itemListCode).getItemName().equals(stageInfoList.get(stageInfoListCode).getMain())) {
                rawData.setMain(stageInfoList.get(stageInfoListCode).getMain());
                rawData.setType(stageInfoList.get(stageInfoListCode).getType());
            }
            rawData.setSecondary(stageInfoList.get(stageInfoListCode).getSecondary());
            rawData.setSecondaryId(stageInfoList.get(stageInfoListCode).getSecondaryId());
            rawData.setColor(2);
            rawData.setUpdateDate(updateTime);
            rawData.setSpm(String.valueOf(stageInfoList.get(stageInfoListCode).getSpm()));
            rawData.setIsShow(stageInfoList.get(stageInfoListCode).getIsShow());
            rawData.setIsValue(stageInfoList.get(stageInfoListCode).getIsValue());
            rawData.setIsSpecial(stageInfoList.get(stageInfoListCode).getIsSpecial());
            rawData.setIsOpen(stageInfoList.get(stageInfoListCode).getIsOpen());
            rawData.setActivityName(exItem);
            rawData.setRequest(request);
            rawData.setEfficiencyEx(efficiencyEx);
            rawDataList.add(rawData);


            for (int k = rawDataListCode; k < rawDataList.size(); k++) {
                rawDataList.get(k).setEfficiency(efficiency);
                rawDataList.get(k).setEfficiencyEx(efficiencyEx);
                rawDataList.get(k).setActivityName(exItem);
                rawDataList.get(k).setConfidence(confidenceInterval);
            }
        }


        HashMap<String, List<PenguinData>> rawDataHashMap = new HashMap<>();

        try {
            for (PenguinData rawData : rawDataList) {

                if (rawData.getMain() != null && rawData.getIsShow() == 1) {

                    if (rawDataHashMap.get(rawData.getType()) == null) {
                        List<PenguinData> rawDataListValue = Collections.singletonList(rawData);
                        rawDataHashMap.put(rawData.getType(), rawDataListValue);
                    } else {
                        List<PenguinData> rawDataListValue = new ArrayList<>(rawDataHashMap.get(rawData.getType()));
                        rawDataListValue.add(rawData);
                        rawDataHashMap.put(rawData.getType(), rawDataListValue);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        HashMap<String, Double> desc = Desc(rawDataHashMap);


        if (num == 2) {
            return rawDataList;
        }

        itemService.itemResvise(desc);

        return rawDataList;
    }


    private static HashMap<String, Double> Desc(HashMap<String, List<PenguinData>> rawDataHashMap) {
        HashMap<String, Double> mianValueNum = new HashMap<>();
        String[] mainName = new String[]{"全新装置", "异铁组", "轻锰矿", "凝胶", "扭转醇", "酮凝集组", "RMA70-12", "炽合金", "研磨石", "糖组",
                "聚酸酯组", "晶体元件", "固源岩组", "半自然溶剂", "化合切削液"};
        for (String itemName : mainName) {
            List<PenguinData> rawDataMapValue = rawDataHashMap.get(itemName);
            rawDataMapValue.sort(comparing(PenguinData::getEfficiency).reversed());
            List<PenguinData> rawDataList = new ArrayList<>();

            int min = 0;
            for (int j = 0; j < rawDataMapValue.size(); j++) {
                if (rawDataMapValue.get(min).getExpect() > rawDataMapValue.get(j).getExpect()) {
                    if(rawDataMapValue.get(min).getExpect() - rawDataMapValue.get(j).getExpect()>2){
                        min = j;
                    }

                }
            }
            if (min == 0) {
                rawDataMapValue.get(0).setColor(4);
//				System.out.println("期望理智最低的是第一个" + rawDataMapValue.get(0).getStageName());
            } else {
                rawDataMapValue.get(0).setColor(3);
                rawDataMapValue.get(min).setColor(1);
//				System.out.println("期望理智最低的是其他" + rawDataMapValue.get(min).getStageName());
            }

//			System.out.println("期望理智最低的是" + min);
            for (PenguinData penguinData : rawDataMapValue) {
                if (penguinData.getIsValue() == 1) {
                    rawDataList.add(penguinData);
                }
            }


            mianValueNum.put(itemName, 1.25 / rawDataList.get(0).getEfficiency());
			System.out.println(rawDataList.get(0).getStageName()+"的效率是"+rawDataList.get(0).getEfficiency());
        }


        return mianValueNum;
    }

    private static String readJsonFile(String fileName) {
        String jsonStr = "";
        try {
            File jsonFile = new File(fileName);

            Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }

            reader.close();
            jsonStr = sb.toString();
            return jsonStr;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}



