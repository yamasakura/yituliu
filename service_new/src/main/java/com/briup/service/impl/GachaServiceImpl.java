package com.briup.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import com.briup.bean.gacha.*;
import com.briup.common.util.GetUtils;
import com.briup.common.util.PostUtils;
import com.briup.dao.GachaDao;
import com.briup.dao.GachaResultVoDao;
import com.briup.dao.UserDataDao;
import com.briup.service.GachaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class GachaServiceImpl implements GachaService {

    @Autowired
    private GachaDao gachaDao;

    @Autowired
    private UserDataDao userDataDao;

    @Autowired
    private GachaResultVoDao gachaResultVoDao;

    @Override
    public Map<String, String> saveGachaData(String userToken) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
        String userName = "yama";
        List<GachaData> gachaDatasVoList = new ArrayList<GachaData>();
        Set<String> poolNameSet = new HashSet<String>();

//      请求参数
        JsonRep jsonRep = new JsonRep();
        jsonRep.setAppId(1);
        jsonRep.setChannelMasterId(1);
        jsonRep.setChannelToken(new ChannelToken(userToken.replace("\"", "")));

        //token GBK编码转化
        String token = getURLEncoderString(userToken.replace("\"", ""));

        //请求api返回结果
        String jsonStr = JSON.toJSONString(jsonRep);
        JSONObject jsonObject = JSONObject.parseObject(jsonStr);
        String postBody = null;
        try {
            postBody = PostUtils.PostBody("https://as.hypergryph.com/u8/user/info/v1/basic", jsonObject);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //拿到uid和昵称
        JSONObject jsonPostBody = JSONObject.parseObject(postBody);
        JSONObject postBodyData = JSONObject.parseObject(JSON.toJSONString(jsonPostBody.get("data")));
        String nickName = JSON.toJSONString(postBodyData.get("nickName")).replace("\"", "");
        String uid = JSON.toJSONString(postBodyData.get("uid")).replace("\"", "");
        System.out.println(nickName);
        System.out.println(uid);
        userName = nickName;

        //获取页数总量
        String ulr_head = "https://ak.hypergryph.com/user/api/inquiry/gacha?page=";
        String url = "https://ak.hypergryph.com/user/api/inquiry/gacha?page=1&token=" + token;
        String con = GetUtils.GetBody(url);
        JSONObject obj = JSONObject.parseObject(con);
        JSONObject data = JSONObject.parseObject(JSON.toJSONString(obj.get("data")));
        JSONObject pagination = JSONObject.parseObject(JSON.toJSONString(data.get("pagination")));
        int current = Integer.parseInt(JSON.toJSONString(pagination.get("total")));
        Integer pageTotal = 0;
        if (current % 10 == 0)
            pageTotal = current / 10;
        else
            pageTotal = (current / 10) + 1;


//          循环请求获取寻访的json数据
        for (int i = 1; i <= pageTotal; i++) {
            url = ulr_head + i + "&token=" + token;

            con = GetUtils.GetBody(url);
            obj = JSONObject.parseObject(con);
            data = JSONObject.parseObject(JSON.toJSONString(obj.get("data")));
            List<GachaClass> gachaList = new ArrayList<GachaClass>();
            gachaList = JSONObject.parseArray(JSON.toJSONString(data.get("list")), GachaClass.class);


            for (int j = 0; j < gachaList.size(); j++) {
                for (int m = 0; m < gachaList.get(j).getChars().size(); m++) {
                    poolNameSet.add(gachaList.get(j).getPool());
                    GachaData gachaData = new GachaData();

                    gachaData.setName(gachaList.get(j).getChars().get(m).getName());
                    gachaData.setGachaId(uid + "_" + gachaList.get(j).getTs() + "_" + m);
                    gachaData.setRarity(gachaList.get(j).getChars().get(m).getRarity());
                    gachaData.setNew(gachaList.get(j).getChars().get(m).isNew());
                    gachaData.setPool(gachaList.get(j).getPool());
                    int time = Integer.parseInt(gachaList.get(j).getTs());
                    gachaData.setTs(simpleDateFormat.format(new Date(time * 1000L)).toString());

//                    System.out.println("页码数" + i + "---" + gachaData);
                    gachaDatasVoList.add(gachaData);
                }

            }
        }




        gachaDao.saveAll(gachaDatasVoList);


        HashMap<String, String> map = new HashMap<>();
        map.put("nikeName", nickName);
        map.put("uid", uid);

        return map;
    }


    @Override
    public GachaVo findById(String uid) {

        List<UserData> userDataList = new ArrayList<>();

        List<GachaData> gachaDatasVoList = gachaDao.findAllByGachaIdLikeOrderByGachaIdAsc(uid+ "%");

        Set<String> poolNameSet = new HashSet<String>();
        for(GachaData gachaData:gachaDatasVoList){
            poolNameSet.add(gachaData.getPool());
        }

        List<GachaResultVo> gachaR6List = new ArrayList<>();

        for (String name : poolNameSet) {

            UserData userData = new UserData();
            Double r6 = 0.0;
            Double r5 = 0.0;
            Double r4 = 0.0;
            Double r3 = 0.0;
            int lastNew = 0;
            int lastCount = 0;
            int r6Times=0;
            for (GachaData gachaDate : gachaDatasVoList) {

                if (name.equals(gachaDate.getPool())) {
                    r6Times++;
                    System.out.println(gachaDate+"次数"+r6Times);
                    GachaResultVo gachaResultVo = new GachaResultVo();
                    gachaResultVo.setTsUid(gachaDate.getGachaId());
                    gachaResultVo.setPool(name);
                    gachaResultVo.setName(gachaDate.getName());
                    gachaResultVo.setRarity(gachaDate.getRarity());
                    gachaResultVo.setNew(gachaDate.isNew());
                    gachaResultVo.setTimes(r6Times);
                    gachaResultVo.setTs(gachaDate.getTs());
                    lastCount++;
                    lastNew++;

                    if (gachaDate.getRarity() == 5) {
                        gachaR6List.add(gachaResultVo);
                        r6Times=0;
                        r6++;
                       lastNew=0;
                    } else if (gachaDate.getRarity() == 4) {
                        r5++;
                    } else if (gachaDate.getRarity() == 3) {
                        r4++;
                    } else if (gachaDate.getRarity() == 2) {
                        r3++;
                    }
                }
            }



            userData.setAvgR6(r6);
            userData.setAvgR5(r5);
            userData.setAvgR4(r4);
            userData.setAvgR3(r3);



            System.out.println("最后一次"+ lastNew);
            if (userDataDao.findAllByUid(uid).get(0).getPoolUid() != null) {
                userData.setBeginTime(gachaDatasVoList.get(gachaDatasVoList.size() - 1).getTs());
            } else {
                userData.setBeginTime(userDataDao.findAllByUid(uid).get(0).getBeginTime());
            }


            userData.setEndTime(gachaDatasVoList.get(0).getTs());
            userData.setPool(name);
            userData.setPoolUid(name + uid);
            userData.setUid(uid);
            userData.setTotal(lastCount);
            userData.setLastNew(lastNew);
            userDataList.add(userData);


        }


        gachaResultVoDao.saveAll(gachaR6List);

        userDataDao.saveAll(userDataList);


        int total = 0;
        Double r6 = 0.0;
        Double r5 = 0.0;
        Double r4 = 0.0;
        Double r3 = 0.0;

        GachaVo gachaVo = new GachaVo();
        List<PoolInfo> poolInfoList = new ArrayList<>();
        List<GachaResultVo> gachaResultVoList = new ArrayList<>();

//        List<UserData> userDataList = userDataDao.findAllByUid(uid);


        for (UserData userData : userDataList) {
            total = total + userData.getTotal();
            r6 = r6 + userData.getAvgR6();
            r5 = r5 + userData.getAvgR5();
            r4 = r4 + userData.getAvgR4();
            r3 = r3 + userData.getAvgR3();
            PoolInfo poolInfo = new PoolInfo();
            poolInfo.setTotal(userData.getTotal());
            poolInfo.setLastNew(userData.getLastNew());
            poolInfo.setPool(userData.getPool());
            poolInfoList.add(poolInfo);
//            List<GachaResultVo>  gachaResultVos= JSONArray.parseArray(userData.getGachaR6List(),GachaResultVo.class);
//            gachaResultVoList.addAll(gachaResultVos);

        }

        List<GachaResultVo> byUidOrderByTsDesc = gachaResultVoDao.findByTsUidLikeOrderByTsDesc(uid + "%");


        gachaVo.setTotal(total);
        gachaVo.setAvgR6(r6);
        gachaVo.setAvgR5(r5);
        gachaVo.setAvgR4(r4);
        gachaVo.setAvgR3(r3);
        gachaVo.setPoolInfo(poolInfoList);
        gachaVo.setBeginAndEndTime(userDataList.get(0).getBeginTime() + userDataList.get(0).getEndTime());
        gachaVo.setGachaResultVo(byUidOrderByTsDesc);
        return gachaVo;
    }



    @Override
    public void test() {
        Long count = 0L;

        String[] character_table = new String[]{
                "char_285_medic2", "char_286_cast3", "char_376_therex", "char_4000_jnight",
                "char_502_nblade", "char_500_noirc", "char_503_rang", "char_501_durin",
                "char_009_12fce", "char_123_fang", "char_240_wyvern", "char_504_rguard",
                "char_192_falco", "char_208_melan", "char_281_popka", "char_209_ardign",
                "char_122_beagle", "char_284_spot", "char_124_kroos", "char_211_adnach",
                "char_507_rsnipe", "char_121_lava", "char_120_hibisc", "char_212_ansel",
                "char_506_rmedic", "char_210_stward", "char_505_rcast", "char_278_orchid",
                "char_141_nights", "char_109_fmout", "char_253_greyy", "char_328_cammou",
                "char_469_indigo", "char_4004_pudd", "char_235_jesica", "char_126_shotst",
                "char_190_clour", "char_133_mm", "char_118_yuki", "char_440_pinecn",
                "char_302_glaze", "char_366_acdrop", "char_198_blackd", "char_149_scave",
                "char_290_vigna", "char_151_myrtle", "char_452_bstalk", "char_130_doberm",
                "char_289_gyuki", "char_159_peacok", "char_193_frostl", "char_127_estell",
                "char_185_frncat", "char_301_cutter", "char_337_utage", "char_271_spikes",
                "char_237_gravel", "char_272_strong", "char_236_rope", "char_117_myrrh",
                "char_187_ccheal", "char_298_susuro", "char_181_flower", "char_385_finlpp",
                "char_199_yak", "char_150_snakek", "char_381_bubble", "char_196_sunbr",
                "char_260_durnar", "char_110_deepcl", "char_183_skgoat", "char_258_podego",
                "char_484_robrta", "char_355_ethan", "char_277_sqrrel", "char_128_plosis",
                "char_275_breeze", "char_115_headbr", "char_102_texas", "char_349_chiave",
                "char_261_sddrag", "char_496_wildmn", "char_401_elysm", "char_308_swire",
                "char_265_sophia", "char_106_franka", "char_131_flameb", "char_508_aguard",
                "char_155_tiger", "char_415_flint", "char_140_whitew", "char_294_ayer",
                "char_252_bibeak", "char_459_tachak", "char_143_ghost", "char_356_broca",
                "char_274_astesi", "char_333_sidero", "char_475_akafyu", "char_421_crow",
                "char_486_takila", "char_129_bluep", "char_204_platnm", "char_367_swllow",
                "char_511_asnipe", "char_365_aprl", "char_219_meteo", "char_379_sesa",
                "char_279_excu", "char_346_aosta", "char_002_amiya", "char_405_absin",
                "char_411_tomimi", "char_166_skfire", "char_509_acast", "char_306_leizi",
                "char_344_beewax", "char_373_lionhd", "char_388_mint", "char_338_iris",
                "char_1011_lava2", "char_489_serum", "char_4013_kjera", "char_242_otter",
                "char_336_folivo", "char_108_silent", "char_171_bldsk", "char_345_folnic",
                "char_510_amedic", "char_348_ceylon", "char_436_whispr", "char_402_tuye",
                "char_473_mberry", "char_449_glider", "char_148_nearl", "char_226_hmau",
                "char_144_red", "char_243_waaifu", "char_214_kafka", "char_455_nothin",
                "char_107_liskam", "char_201_moeshd", "char_325_bison", "char_163_hpsts",
                "char_378_asbest", "char_512_aprot", "char_4025_aprot2", "char_457_blitz",
                "char_304_zebra", "char_431_ashlok", "char_422_aurora", "char_145_prove",
                "char_158_milu", "char_218_cuttle", "char_363_toddi", "char_173_slchan",
                "char_383_snsant", "char_174_slbell", "char_254_vodfox", "char_195_glassb",
                "char_326_glacus", "char_101_sora", "char_343_tknogi", "char_4019_ncdeer",
                "char_215_mantic", "char_478_kirara", "char_241_panda", "char_451_robin",
                "char_458_rfrost", "char_369_bena", "char_103_angel", "char_332_archet",
                "char_456_ash", "char_340_shwaz", "char_430_fartth", "char_113_cqbw",
                "char_197_poca", "char_391_rosmon", "char_1013_chen2", "char_112_siege",
                "char_222_bpipe", "char_362_saga", "char_479_sleach", "char_420_flamtl",
                "char_134_ifrit", "char_213_mostma", "char_180_amgoat", "char_2013_cerber",
                "char_2015_dusk", "char_472_pasngr", "char_426_billro", "char_206_gnosis",
                "char_291_aglina", "char_358_lisa", "char_248_mgllan", "char_1012_skadi2",
                "char_250_phatom", "char_400_weedy", "char_225_haak", "char_474_glady",
                "char_437_mizuki", "char_147_shining", "char_179_cgbird", "char_003_kalts",
                "char_136_hsguma", "char_202_demkni", "char_423_blemsh", "char_2014_nian",
                "char_311_mudrok", "char_416_zumama", "char_264_f12yin", "char_172_svrash",
                "char_293_thorns", "char_010_chen", "char_017_huang", "char_350_surtr",
                "char_188_helage", "char_485_pallas", "char_1014_nearl2", "char_230_savage",
                "char_282_catap", "char_283_midn", "char_137_brownb", "char_347_jaksel",
                "char_164_nightm", "char_220_grani", "char_263_skadi", "char_2023_ling",
                "char_322_lmlee", "char_1021_kroos2", "char_476_blkngt"
        };

        for (int i = 0; i < 600000; i++) {
            List<GachaData> gachaDatasVoList = new ArrayList<GachaData>();
            for (int j = 0; j < 200; j++) {
                GachaData gachaData = new GachaData();
                gachaData.setGachaId(("817087451" + j + i));
                gachaData.setRarity((int)(Math.random()*(2+1-5)+5));
                gachaData.setName(character_table[j]);
                gachaData.setNew(false);
                gachaData.setPool("浊酒澄心");
                gachaData.setTs("2022-01-25 16:02:42");
                gachaDatasVoList.add(gachaData);
                count++;
            }


//            try {
//                Thread.sleep(3000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

            gachaDao.saveAll(gachaDatasVoList);
            System.out.println("目标量：" + 6000000 + "，当前量：" + count + "，差额为：" + (6000000 - count));
        }

    }

    @Override
    public HashMap<String, Integer> findAllData(String uid) {
        HashMap<String, Integer> map = new HashMap<>();

        Integer integer = gachaDao.countSize();

        for (int i = 0; i < integer; i += 1000) {


            List<GachaData> allData = gachaDao.findByGachaIdLimit(uid, i);


            for (GachaData gachaData : allData) {
                if (map.containsKey(gachaData.getName())) {
                    int count = map.get(gachaData.getName());
                    map.put(gachaData.getName(),count+1);

                } else {
                    map.put(gachaData.getName(), 1);
                }
            }
            String s = JSON.toJSONString(map);
            System.out.println(s);
        }




        return map;
    }

    private final static String ENCODE = "GBK";

    public static String getURLEncoderString(String str) {
        String result = "";
        if (null == str) {
            return "";
        }
        try {
            result = java.net.URLEncoder.encode(str, ENCODE);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }


}



