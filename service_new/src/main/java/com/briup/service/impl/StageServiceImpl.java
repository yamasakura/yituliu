package com.briup.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.briup.bean.dataBean.Stage;
import com.briup.bean.vo.StageVo;
import com.briup.dao.StageDao;
import com.briup.service.StageService;
import java.net.URLEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class StageServiceImpl implements StageService {

    @Autowired
    private StageDao stageDao;

    @Override
    public List<Stage> findAll() {
        List<Stage> all = stageDao.findAll();

        List<Stage> allStage = new ArrayList<>();

        for (int i = 0; i < all.size(); i++) {
            Stage stage = new Stage();
            stage.setStageEnName(all.get(i).getStageEnName());
            stage.setStageName(all.get(i).getStageName());
            stage.setMain(all.get(i).getMain());
            stage.setReason(all.get(i).getReason());
            stage.setSecondary(all.get(i).getSecondary());
            stage.setSecondaryId(all.get(i).getSecondaryId());
            stage.setIsSpecial(all.get(i).getIsSpecial());
            stage.setIsOpen(all.get(i).getIsOpen());
            stage.setStageId(all.get(i).getStageId());
            stage.setType(all.get(i).getType());
            stage.setIsShow(all.get(i).getIsShow());
            stage.setSpm(all.get(i).getSpm());
            stage.setIsValue(all.get(i).getIsValue());
            stage.setChapterCode(all.get(i).getChapterCode());
            stage.setChapterName(all.get(i).getChapterName());
            allStage.add(stage);
        }
        return allStage;
    }

    @Override
    public List<StageVo> findAllEx() {
        List<Stage> all = stageDao.findAll();

        List<StageVo> allStage = new ArrayList<>();

        for (int i = 0; i < all.size(); i++) {
            StageVo stage = new StageVo();
            stage.setStageEnName(all.get(i).getStageEnName());
            stage.setStageName(all.get(i).getStageName());
            stage.setMain(all.get(i).getMain());
            stage.setReason(Double.valueOf(all.get(i).getReason()));
            stage.setSecondary(all.get(i).getSecondary());
            stage.setSecondaryId(all.get(i).getSecondaryId());
            stage.setIsSpecial(all.get(i).getIsSpecial());
            stage.setIsOpen(all.get(i).getIsOpen());
            stage.setStageId(all.get(i).getStageId());
            stage.setType(all.get(i).getType());
            stage.setIsShow(all.get(i).getIsShow());
            stage.setSpm(all.get(i).getSpm());
            stage.setIsValue(all.get(i).getIsValue());
            stage.setChapterCode(all.get(i).getChapterCode());
            stage.setChapterName(all.get(i).getChapterName());
            stage.setReasonEx(Double.valueOf(all.get(i).getReason()));
            allStage.add(stage);
        }
        return allStage;
    }

    @Override
    public void importStageData(MultipartFile file) {

        List<Stage> list = new ArrayList<>();

        try {
            EasyExcel.read(file.getInputStream(), Stage.class, new AnalysisEventListener<Stage>() {

                @Override
                public void invoke(Stage stage, AnalysisContext analysisContext) {
                    list.add(stage);
                }

                @Override
                public void doAfterAllAnalysed(AnalysisContext analysisContext) {

                }
            }).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }

        stageDao.saveAll(list);

    }

    @Override
    public void exportStageData(HttpServletResponse response) {

        try {
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
// 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("stageData", "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename="+ fileName+".xlsx" );

            List<Stage> list = stageDao.findAll();

            EasyExcel.write(response.getOutputStream(), Stage.class).sheet("Sheet1").doWrite(list);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
