package com.briup.service.impl;


import com.briup.bean.gacha.UserData;
import com.briup.dao.UserDataDao;
import com.briup.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDataServiceImpl implements UserDataService {

    @Autowired
    private UserDataDao userDataDao;

    @Override
    public void saveAll(List<UserData> userDataList) {
         userDataDao.saveAll(userDataList);


    }
}
