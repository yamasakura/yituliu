package com.briup.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.briup.bean.ItemRevise;
import com.briup.bean.StoreCostPer;
import com.briup.bean.dataBean.StoreCostList;
import com.briup.common.exception.ServiceException;
import com.briup.common.util.ResultCode;
import com.briup.dao.StoreCostPerDao;
import com.briup.service.ItemService;
import com.briup.service.StoreCostPerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


@Service
public class StoreCostPerServiceImpl implements StoreCostPerService {
	
	@Autowired
	private  StoreCostPerDao storeCostPerDao ;

	@Autowired
	private ItemService itemService ;

	@Autowired
	private StoreCostPerService storeCostPerService ;



	@Override
	public void save(List<StoreCostPer> storeCostPer) {
		if(storeCostPer!=null) {
			storeCostPerDao.saveAll(storeCostPer);
		}else {
			throw new ServiceException(ResultCode.PARAM_IS_BLANK);
		}

	}

	@Override
	public void update(MultipartFile file) {


		List<ItemRevise> allItem =itemService.findAllItemRevise();

		List<StoreCostList> list = new ArrayList<>();

		try {
			EasyExcel.read(file.getInputStream(), StoreCostList.class, new AnalysisEventListener<StoreCostList>() {

				@Override
				public void invoke(StoreCostList storeCostList, AnalysisContext analysisContext) {
					list.add(storeCostList);
				}

				@Override
				public void doAfterAllAnalysed(AnalysisContext analysisContext) {
				}

			}).sheet().doRead();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String[][] storeItemData = new String[list.size()][3];

		for(int i=0;i<list.size();i++){
			storeItemData[i][0]=list.get(i).getItemName();
			storeItemData[i][1]=list.get(i).getCost();
			storeItemData[i][2]=list.get(i).getType();
		}




		DecimalFormat dfbfb = new DecimalFormat("0.00");

		List<StoreCostPer> List = new ArrayList<>();
		List<StoreCostPer> actList = new ArrayList<>();
		Long count = 1L;



		for (int i = 0; i < storeItemData.length; i++) {
			for (int j = 0; j < allItem.size(); j++) {
				if (storeItemData[i][0].equals(allItem.get(j).getItemName())) {
					Double costPer = allItem.get(j).getItemValue() / Double.parseDouble(storeItemData[i][1]);
					StoreCostPer storeCostPer = new StoreCostPer();
					storeCostPer.setId(count);
					count++;
					storeCostPer.setItemId(allItem.get(j).getItemId());
					storeCostPer.setItemName(storeItemData[i][0]);
					storeCostPer.setStoreType(storeItemData[i][2]);
					storeCostPer.setCostPer(Double.valueOf(dfbfb.format(costPer)));
					storeCostPer.setCost(Double.valueOf(storeItemData[i][1]));
					storeCostPer.setItemValue(allItem.get(j).getItemValue());
					List.add(storeCostPer);
				}
			}
		}

		storeCostPerService.save(List);



	}


	@Override
	public List<StoreCostPer> findAll(String type) {
		
		 List<StoreCostPer> storeAllData = storeCostPerDao.findByStoreTypeOrderByCostPerDesc(type);
		
		return storeAllData;
	}




	private List<StoreCostPer> desc(List<StoreCostPer> storeCostPers){

		StoreCostPer[] storeCostPersStr = new StoreCostPer[storeCostPers.size()];
		for (int i = 0; i < storeCostPers.size(); i++) {
			storeCostPersStr[i] = storeCostPers.get(i);
		}



		for (int i = 0; i < storeCostPers.size()-1; i++) {
			StoreCostPer storeCostPer = new StoreCostPer();
			for (int j = 0; j < storeCostPers.size()-1-i; j++) {
				if(storeCostPersStr[j].getCostPer()<storeCostPersStr[j+1].getCostPer()){
					storeCostPer = storeCostPersStr[j];
					storeCostPersStr[j] = storeCostPersStr[j+1];
					storeCostPersStr[j+1] =storeCostPer;
				}
			}
		}

		storeCostPers.clear();
		for (int i = 0; i < storeCostPersStr.length ; i++) {
			storeCostPers.add(storeCostPersStr[i]);
		}


		return storeCostPers;
	}


}
