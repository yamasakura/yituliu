package com.briup.service.impl;

import com.alibaba.excel.EasyExcel;
import com.briup.bean.ItemRevise;
import com.briup.bean.dataBean.Item;

import com.briup.bean.dataBean.Stage;
import com.briup.dao.ItemDao;
import com.briup.dao.ItemReviseDao;
import com.briup.mapper.ItemMapper;
import com.briup.mapper.ItemReviseMapper;
import com.briup.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemDao itemDao;
    @Autowired
    private ItemReviseDao itemReviseDao;


    public void item() {
        HashMap<String, Double> itemValue = new HashMap<>();
        String item[][] = getItemInfo();
        double avgItemValue_t1 = (1.667 * 0.263 + 2.5 * 0.175 + 2.5 * 0.175 + 2.92 * 0.14 + 2.92 * 0.14 + 3.75
                * 0.105) * 0.2 - 0.45;

        double avgItemValue_t2 = (5.0 * 0.263 + 7.5 * 0.175 + 7.5 * 0.175 + 8.75 * 0.14 + 8.75 * 0.14 + 11.25
                * 0.105) * 0.2 - 0.9;

        double avgItemValue_t3 = (0.099 * 25 + 0.082 * 30 + 0.082 * 30 + 0.066 * 35 + 0.066 * 35 + 0.049 * 45 + 0.074 * 30
                + 0.066 * 35 + 0.059 * 40 + 0.049 * 45 + 0.059 * 40 + 0.066 * 35 + 0.066 * 30 + 0.059 * 40
                + 0.059 * 40) * 0.2 - 1.35;

        double avgItemValue_t4 = (0.09 * 100 + 0.067 * 130 + 0.067 * 125 + 0.06 * 145 + 0.06 * 130 + 0.045 * 135 + 0.077 * 105 +
                0.067 * 130 + 0.067 * 120 + 0.06 * 130 + 0.077 * 110 + 0.067 * 120 + 0.067 * 135 + 0.067 * 115 + 0.067 * 120) * 0.2 - 1.8;


        itemValue.put("固源岩组", 25.0);
        itemValue.put("糖组", 30.0);
        itemValue.put("聚酸酯组", 30.0);
        itemValue.put("异铁组", 35.0);
        itemValue.put("酮凝集组", 35.0);
        itemValue.put("全新装置", 45.0);
        itemValue.put("扭转醇", 30.0);
        itemValue.put("轻锰矿", 35.0);
        itemValue.put("研磨石", 40.0);
        itemValue.put("RMA70-12", 45.0);
        itemValue.put("凝胶", 40.0);
        itemValue.put("炽合金", 35.0);
        itemValue.put("晶体元件", 30.0);
        itemValue.put("半自然溶剂", 40.0);
        itemValue.put("化合切削液", 40.0);

        itemValue.put("固源岩", (itemValue.get("固源岩组") + avgItemValue_t2) / 5);
        itemValue.put("糖", (itemValue.get("糖组") + avgItemValue_t2) / 4);
        itemValue.put("聚酸酯", (itemValue.get("聚酸酯组") + avgItemValue_t2) / 4);
        itemValue.put("异铁", (itemValue.get("异铁组") + avgItemValue_t2) / 4);
        itemValue.put("酮凝集", (itemValue.get("酮凝集组") + avgItemValue_t2) / 4);
        itemValue.put("装置", (itemValue.get("全新装置") + avgItemValue_t2) / 4);

        itemValue.put("源岩", (itemValue.get("固源岩") + avgItemValue_t1) / 3);
        itemValue.put("代糖", (itemValue.get("糖") + avgItemValue_t1) / 3);
        itemValue.put("酯原料", (itemValue.get("聚酸酯") + avgItemValue_t1) / 3);
        itemValue.put("异铁碎片", (itemValue.get("异铁") + avgItemValue_t1) / 3);
        itemValue.put("双酮", (itemValue.get("酮凝集") + avgItemValue_t1) / 3);
        itemValue.put("破损装置", (itemValue.get("装置") + avgItemValue_t1) / 3);

        itemValue.put("白马醇", itemValue.get("糖组") * 1 + itemValue.get("扭转醇") + itemValue.get("RMA70-12") - avgItemValue_t3);
        itemValue.put("三水锰矿", itemValue.get("轻锰矿") * 2 + itemValue.get("扭转醇") + itemValue.get("聚酸酯组") - avgItemValue_t3);
        itemValue.put("五水研磨石", itemValue.get("研磨石") * 1 + itemValue.get("异铁组") + itemValue.get("全新装置") - avgItemValue_t3);
        itemValue.put("RMA70-24", itemValue.get("固源岩组") * 2 + itemValue.get("RMA70-12") + itemValue.get("酮凝集组") - avgItemValue_t3);
        itemValue.put("改量装置", itemValue.get("固源岩组") * 2 + itemValue.get("全新装置") + itemValue.get("研磨石") - avgItemValue_t3);
        itemValue.put("聚酸酯块", itemValue.get("聚酸酯组") * 2 + itemValue.get("酮凝集组") + itemValue.get("扭转醇") - avgItemValue_t3);
        itemValue.put("糖聚块", itemValue.get("糖组") * 2 + itemValue.get("异铁组") + itemValue.get("轻锰矿") - avgItemValue_t3);
        itemValue.put("异铁块", itemValue.get("异铁组") * 2 + itemValue.get("全新装置") + itemValue.get("聚酸酯组") - avgItemValue_t3);
        itemValue.put("酮阵列", itemValue.get("酮凝集组") * 2 + itemValue.get("糖组") + itemValue.get("轻锰矿") - avgItemValue_t3);
        itemValue.put("聚合凝胶", itemValue.get("异铁组") * 1 + itemValue.get("凝胶") + itemValue.get("炽合金") - avgItemValue_t3);
        itemValue.put("炽合金块", itemValue.get("炽合金") * 1 + itemValue.get("全新装置") + itemValue.get("研磨石") - avgItemValue_t3);
        itemValue.put("晶体电路", itemValue.get("晶体元件") * 2 + itemValue.get("凝胶") + itemValue.get("炽合金") - avgItemValue_t3);
        itemValue.put("精炼溶剂", itemValue.get("半自然溶剂") * 1 + itemValue.get("化合切削液") + itemValue.get("凝胶") - avgItemValue_t3);
        itemValue.put("切削原液", itemValue.get("化合切削液") * 1 + itemValue.get("晶体元件") + itemValue.get("凝胶") - avgItemValue_t3);
        itemValue.put("提纯源岩", itemValue.get("固源岩组") * 4 - avgItemValue_t3);

        itemValue.put("D32钢", itemValue.get("三水锰矿") * 2 + itemValue.get("五水研磨石") + itemValue.get("RMA70-24") - avgItemValue_t4);
        itemValue.put("双极纳米片", itemValue.get("白马醇") * 1 + itemValue.get("白马醇") + itemValue.get("改量装置") - avgItemValue_t4);
        itemValue.put("聚合剂", itemValue.get("提纯源岩") * 1 + itemValue.get("异铁块") + itemValue.get("酮阵列") - avgItemValue_t4);
        itemValue.put("晶体电子单元", itemValue.get("聚合凝胶") * 2 + itemValue.get("炽合金块") + itemValue.get("晶体电路") - avgItemValue_t4);


        List<Item> itemList = new ArrayList<>();

        for (String[] str : item) {
            if (itemValue.get(str[1]) != null) {

                Item itemResult = new Item();
                itemResult.setItemId(str[0]);
                itemResult.setItemName(str[1]);
                itemResult.setItemValue(itemValue.get(str[1]));
                itemResult.setType(str[3]);
                itemList.add(itemResult);
            } else {
                Item itemResult = new Item();
                itemResult.setItemId(str[0]);
                itemResult.setItemName(str[1]);
                itemResult.setItemValue(Double.valueOf(str[2]));
                itemResult.setType(str[3]);
                itemList.add(itemResult);
            }
        }

        itemDao.saveAll(itemList);
    }


    @Override
    public List<Item> findAll() {
        List<Item> findAll = itemDao.findAllByOrderByItemIdAsc();
        return findAll;
    }

    @Override
    public List<ItemRevise> findAllItemRevise() {
        List<ItemRevise> all = itemReviseDao.findAll();
        return all;
    }

    @Override
    public List<ItemRevise> itemResvise(HashMap<String, Double> hashMap) {
        String item[][] = getItemInfo();

        double avgItemValue_t1 = 0.513182485 - 0.45;
        double avgItemValue_t2 = 1.538558598 - 0.9;
        double avgItemValue_t3 = 6.937 - 1.35;
        double avgItemValue_t4 = 23.4191630 - 1.8;

        HashMap<String, Double> itemValue = new HashMap<>();
        for (Map.Entry<String, Double> entry : hashMap.entrySet()) {
//            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
        itemValue.put("固源岩组", hashMap.get("固源岩组") * 25);
        itemValue.put("糖组", hashMap.get("糖组") * 30);
        itemValue.put("聚酸酯组", hashMap.get("聚酸酯组") * 30);
        itemValue.put("异铁组", hashMap.get("异铁组") * 35);
        itemValue.put("酮凝集组", hashMap.get("酮凝集组") * 35);
        itemValue.put("全新装置", hashMap.get("全新装置") * 45);
        itemValue.put("扭转醇", hashMap.get("扭转醇") * 30);
        itemValue.put("轻锰矿", hashMap.get("轻锰矿") * 35);
        itemValue.put("研磨石", hashMap.get("研磨石") * 40);
        itemValue.put("RMA70-12", hashMap.get("RMA70-12") * 45);
        itemValue.put("凝胶", hashMap.get("凝胶") * 40);
        itemValue.put("炽合金", hashMap.get("炽合金") * 35);
        itemValue.put("晶体元件", hashMap.get("晶体元件") * 30);
        itemValue.put("半自然溶剂", hashMap.get("半自然溶剂") * 40);
        itemValue.put("化合切削液", hashMap.get("化合切削液") * 40);

        itemValue.put("固源岩", (itemValue.get("固源岩组") + avgItemValue_t2) / 5);
        itemValue.put("糖", (itemValue.get("糖组") + avgItemValue_t2) / 4);
        itemValue.put("聚酸酯", (itemValue.get("聚酸酯组") + avgItemValue_t2) / 4);
        itemValue.put("异铁", (itemValue.get("异铁组") + avgItemValue_t2) / 4);
        itemValue.put("酮凝集", (itemValue.get("酮凝集组") + avgItemValue_t2) / 4);
        itemValue.put("装置", (itemValue.get("全新装置") + avgItemValue_t2) / 4);

        itemValue.put("源岩", (itemValue.get("固源岩") + avgItemValue_t1) / 3);
        itemValue.put("代糖", (itemValue.get("糖") + avgItemValue_t1) / 3);
        itemValue.put("酯原料", (itemValue.get("聚酸酯") + avgItemValue_t1) / 3);
        itemValue.put("异铁碎片", (itemValue.get("异铁") + avgItemValue_t1) / 3);
        itemValue.put("双酮", (itemValue.get("酮凝集") + avgItemValue_t1) / 3);
        itemValue.put("破损装置", (itemValue.get("装置") + avgItemValue_t1) / 3);

        itemValue.put("白马醇", itemValue.get("糖组") * 1 + itemValue.get("扭转醇") + itemValue.get("RMA70-12") - avgItemValue_t3);
        itemValue.put("三水锰矿", itemValue.get("轻锰矿") * 2 + itemValue.get("扭转醇") + itemValue.get("聚酸酯组") - avgItemValue_t3);
        itemValue.put("五水研磨石", itemValue.get("研磨石") * 1 + itemValue.get("异铁组") + itemValue.get("全新装置") - avgItemValue_t3);
        itemValue.put("RMA70-24", itemValue.get("固源岩组") * 2 + itemValue.get("RMA70-12") + itemValue.get("酮凝集组") - avgItemValue_t3);
        itemValue.put("改量装置", itemValue.get("固源岩组") * 2 + itemValue.get("全新装置") + itemValue.get("研磨石") - avgItemValue_t3);
        itemValue.put("聚酸酯块", itemValue.get("聚酸酯组") * 2 + itemValue.get("酮凝集组") + itemValue.get("扭转醇") - avgItemValue_t3);
        itemValue.put("糖聚块", itemValue.get("糖组") * 2 + itemValue.get("异铁组") + itemValue.get("轻锰矿") - avgItemValue_t3);
        itemValue.put("异铁块", itemValue.get("异铁组") * 2 + itemValue.get("全新装置") + itemValue.get("聚酸酯组") - avgItemValue_t3);
        itemValue.put("酮阵列", itemValue.get("酮凝集组") * 2 + itemValue.get("糖组") + itemValue.get("轻锰矿") - avgItemValue_t3);
        itemValue.put("聚合凝胶", itemValue.get("异铁组") * 1 + itemValue.get("凝胶") + itemValue.get("炽合金") - avgItemValue_t3);
        itemValue.put("炽合金块", itemValue.get("炽合金") * 1 + itemValue.get("全新装置") + itemValue.get("研磨石") - avgItemValue_t3);
        itemValue.put("晶体电路", itemValue.get("晶体元件") * 2 + itemValue.get("凝胶") + itemValue.get("炽合金") - avgItemValue_t3);
        itemValue.put("精炼溶剂", itemValue.get("半自然溶剂") * 1 + itemValue.get("化合切削液") + itemValue.get("凝胶") - avgItemValue_t3);
        itemValue.put("切削原液", itemValue.get("化合切削液") * 1 + itemValue.get("晶体元件") + itemValue.get("凝胶") - avgItemValue_t3);
        itemValue.put("提纯源岩", itemValue.get("固源岩组") * 4 - avgItemValue_t3);

//        itemValue.put("白马醇", 0.0);
//        itemValue.put("三水锰矿", 0.0);
//        itemValue.put("五水研磨石", 0.0);
//        itemValue.put("RMA70-24", 0.0);
//        itemValue.put("改量装置", 0.0);
//        itemValue.put("聚酸酯块",0.0);
//        itemValue.put("糖聚块", 0.0);
//        itemValue.put("异铁块", 0.0);
//        itemValue.put("酮阵列", 0.0);
//        itemValue.put("聚合凝胶", 0.0);
//        itemValue.put("炽合金块", 0.0);
//        itemValue.put("晶体电路", 0.0);
//        itemValue.put("精炼溶剂", 0.0);
//        itemValue.put("切削原液", 0.0);
//        itemValue.put("提纯源岩",0.0);


        itemValue.put("D32钢", itemValue.get("三水锰矿")  + itemValue.get("五水研磨石") + itemValue.get("RMA70-24") - avgItemValue_t4);
        itemValue.put("双极纳米片", itemValue.get("白马醇")  + itemValue.get("白马醇") + itemValue.get("改量装置") - avgItemValue_t4);
        itemValue.put("聚合剂", itemValue.get("提纯源岩") * 1 + itemValue.get("异铁块") + itemValue.get("酮阵列") - avgItemValue_t4);
        itemValue.put("晶体电子单元", itemValue.get("聚合凝胶") * 2 + itemValue.get("炽合金块") + itemValue.get("晶体电路") - avgItemValue_t4);

        List<ItemRevise> itemList = new ArrayList<>();

        Long id = 0L;
        for (String[] str : item) {
            if (itemValue.get(str[1]) != null) {
                ItemRevise itemResult = new ItemRevise();
                itemResult.setId(id);
                itemResult.setItemId(str[0]);
                itemResult.setItemName(str[1]);
                itemResult.setItemValue(itemValue.get(str[1]));
                itemResult.setType(str[3]);
                itemList.add(itemResult);
            } else {
                ItemRevise itemResult = new ItemRevise();
                itemResult.setId(id);
                itemResult.setItemId(str[0]);
                itemResult.setItemName(str[1]);
                itemResult.setItemValue(Double.valueOf(str[2]));
                itemResult.setType(str[3]);
                itemList.add(itemResult);
            }
            id++;
        }
          itemReviseDao.deleteAll();
        itemReviseDao.saveAll(itemList);


        return itemList;
    }

    @Override
    public void exportItemData(HttpServletResponse response) {
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
// 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("itemValue", "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename="+ fileName+".xlsx" );

            List<ItemRevise> list = itemReviseDao.findAll();

            EasyExcel.write(response.getOutputStream(), ItemRevise.class).sheet("Sheet1").doWrite(list);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }





    private static String[][] getItemInfo() {
        String item[][] = new String[][]{
                {"30135", "D32钢", "", "orange"}, {"30125", "双极纳米片", "", "orange"},
                {"30115", "聚合剂", "", "orange"}, {"30145", "晶体电子单元", "", "orange"},
                {"30064", "改量装置", "", "purple"}, {"30044", "异铁块", "", "purple"},
                {"30084", "三水锰矿", "", "purple"}, {"31014", "聚合凝胶", "", "purple"},
                {"30074", "白马醇", "", "purple"}, {"30054", "酮阵列", "", "purple"},
                {"30104", "RMA70-24", "", "purple"}, {"31024", "炽合金块", "", "purple"},
                {"30094", "五水研磨石", "", "purple"}, {"30024", "糖聚块", "", "purple"},
                {"30034", "聚酸酯块", "", "purple"}, {"31034", "晶体电路", "", "purple"},
                {"30014", "提纯源岩", "", "purple"}, {"31044", "精炼溶剂", "", "purple"},
                {"31054", "切削原液", "", "purple"}, {"30063", "全新装置", "25", "blue"},
                {"30043", "异铁组", "30", "blue"}, {"30083", "轻锰矿", "30", "blue"},
                {"31013", "凝胶", "35", "blue"}, {"30073", "扭转醇", "35", "blue"},
                {"30053", "酮凝集组", "45", "blue"}, {"30103", "RMA70-12", "30", "blue"},
                {"31023", "炽合金", "35", "blue"}, {"30093", "研磨石", "40", "blue"},
                {"30023", "糖组", "45", "blue"}, {"30033", "聚酸酯组", "40", "blue"},
                {"31033", "晶体元件", "35", "blue"}, {"30013", "固源岩组", "30", "blue"},
                {"31043", "半自然溶剂", "40", "blue"}, {"31053", "化合切削液", "40", "blue"},
                {"30012", "固源岩", "", "green"}, {"30022", "糖", "", "green"},
                {"30032", "聚酸酯", "", "green"}, {"30042", "异铁", "", "green"},
                {"30052", "酮凝集", "", "green"}, {"30062", "装置", "", "green"},
                {"30011", "源岩", "", "grey"}, {"30021", "代糖", "", "grey"},
                {"30031", "酯原料", "", "grey"}, {"30041", "异铁碎片", "", "grey"},
                {"30051", "双酮", "", "grey"}, {"30061", "破损装置", "", "grey"},
                {"4003", "合成玉", "0.9375", "orange"}, {"7001", "招聘许可", "30.085", "purple"},
                {"4006", "采购凭证", "1.7", "blue"},
                {"7003", "寻访凭证", "562.5", "orange"}, {"3302", "技巧概要·卷2", "5.278", "blue"},
                {"3303", "技巧概要·卷3", "13.196", "green"}, {"3211", "先锋芯片", "21.42", "blue"},
                {"3221", "近卫芯片", "24.99625", "blue"}, {"3231", "重装芯片", "24.99625", "blue"},
                {"3241", "狙击芯片", "21.42", "blue"}, {"3251", "术师芯片", "21.42", "blue"},
                {"3261", "医疗芯片", "17.8425", "blue"}, {"3271", "辅助芯片", "21.42", "blue"},
                {"3281", "特种芯片", "17.8425", "blue"}, {"32001", "芯片助剂", "152.99", "purple"},
                {"3003", "赤金", "1.25", "purple"}, {"3301", "技巧概要·卷1", "2.111", "purple"},
                {"mod_unlock_token", "模组数据块", "204", "orange"}, {"STORY_REVIEW_COIN", "事相碎片", "34", "orange"},
//                   {"2004", "高级作战记录", "6.25", "orange"}, {"2003", "中级作战记录", "3.125", "purple"},
//        {"2002", "初级作战记录", "1.25", "blue"}, {"2001", "基础作战记录", "0.625", "green"},{"4001", "龙门币", "0.005", "purple"},
                        {"2004", "高级作战记录", "5.625", "orange"}, {"2003", "中级作战记录", "2.8125", "purple"},
        {"2002", "初级作战记录", "1.125", "blue"}, {"2001", "基础作战记录", "0.5625", "green"}, {"4001", "龙门币", "0.0045", "purple"},

                		{"ap_supply_lt_010", "应急理智小样",  "0.000001", "purple"},
                 		{"randomMaterial_6", "罗德岛物资补给Ⅲ",  "0.000001", "green"},
        };



        return item;
    }
}
