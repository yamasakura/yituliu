package com.briup.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.briup.bean.PenguinData;
import com.briup.bean.vo.RecResultVo;
import com.briup.bean.dataBean.RoleTagData;
import com.briup.dao.RoleTagDataDao;
import com.briup.service.RoleTagDataService;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;

@Service
public class RoleTagDataServiceImpl implements RoleTagDataService {

    @Autowired
    private RoleTagDataDao roleTagDataDao;

    @Override
    public void saveAll(MultipartFile file) {
        List<RoleTagData> list = new ArrayList<>();

        try {
            EasyExcel.read(file.getInputStream(), RoleTagData.class, new AnalysisEventListener<RoleTagData>() {


                @Override
                public void invoke(RoleTagData roleTagData, AnalysisContext analysisContext) {
                    list.add(roleTagData);
                }

                @Override
                public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                }

            }).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }


        roleTagDataDao.saveAll(list);
    }


    @Override
    public void exportData(HttpServletResponse response) {
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
// 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("recruitmentInfo", "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");


            List<RoleTagData> list = roleTagDataDao.findAll();


            EasyExcel.write(response.getOutputStream(), RoleTagData.class).sheet("Sheet1").doWrite(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<RecResultVo> findAllByTypeAndRarityNew(Integer type, String[] reqTags, Integer rarityMin) {


        int r1Type = 3;
        int r6Type = 5;
        for (int i = 0; i < reqTags.length; i++) {
//            System.out.println(reqTags[i] + " , ");
            if ("高级资深干员".equals(reqTags[i])) {
                r6Type = 6;
            }

        }
        r1Type = rarityMin;

        List<RoleTagData> allData = roleTagDataDao.findByTypeAndRarityBetween(type, r1Type, r6Type);


        HashMap<String, List<String>> hashMap = new HashMap<>();
        for (RoleTagData allRole : allData) {
            StringBuilder tags = new StringBuilder("_");
            Integer sixCode = 0;
            for (String reqTag : reqTags) {
                if (reqTag.equals(allRole.getTag6())) {
                    tags.append(",").append(reqTag);
//                    if("高级资深干员".equals(reqTag)){
//                        sixCode=1;
//                    }else if("资深干员".equals(reqTag)){
//                        sixCode=2;
//                    }

                } else if (reqTag.equals(allRole.getTag5())) {
                    tags.append(",").append(reqTag);
                } else if (reqTag.equals(allRole.getTag4())) {
                    tags.append(",").append(reqTag);
                } else if (reqTag.equals(allRole.getTag3())) {
                    tags.append(",").append(reqTag);
                } else if (reqTag.equals(allRole.getTag2())) {
                    tags.append(",").append(reqTag);
                } else if (reqTag.equals(allRole.getTag1())) {
                    tags.append(",").append(reqTag);
                }
            }

            if ("_".equals(tags.toString())) {
                continue;
            }



            String[] split = tags.toString().split(",");
            for (int i = 1; i < split.length; i++) {
                String tagOne = "";
                tagOne=split[i];
                if((!"高级资深干员".equals(split[i]))&&allRole.getRarity()==6) {
                    System.out.println(split[i]);
                    System.out.println(allRole.getRoleName());
                    continue;
                }
                if (hashMap.get(tagOne) == null) {
                    hashMap.put(tagOne, new ArrayList<>(Collections.singleton(allRole.getRarity() + "_" + allRole.getRoleName())));
                } else {
                    List<String> roleNames = new ArrayList<>(hashMap.get(tagOne));
                    roleNames.add(allRole.getRarity() + "_" + allRole.getRoleName());
                    hashMap.put(tagOne, roleNames);
                }
                for (int j = i + 1; j < split.length; j++) {
                    String tagTwo = "";
                    tagTwo=split[i]+"-"+split[j];
                    System.out.println(split[i]+"-"+split[j]);
                    if((!"高级资深干员".equals(split[i]))&&allRole.getRarity()==6) {
                        System.out.print(split[i]+"-"+split[j]);
                        System.out.println(allRole.getRoleName());
                        continue;
                    }

                    if (hashMap.get(tagTwo) == null) {
                        hashMap.put(tagTwo, new ArrayList<>(Collections.singleton(allRole.getRarity() + "_" + allRole.getRoleName())));
                    } else {
                        List<String> roleNames = new ArrayList<>(hashMap.get(tagTwo));
                        roleNames.add(allRole.getRarity() + "_" + allRole.getRoleName());
                        hashMap.put(tagTwo, roleNames);
                    }
                    for (int k = j + 1; k < split.length; k++) {
                        String tagThree = "";
                        tagThree =split[i]+"-"+split[j]+"-"+split[k];
                        System.out.println(split[i]+"-"+split[j]+"-"+split[k]);
                        if((!"高级资深干员".equals(split[i]))&&allRole.getRarity()==6) {
                            System.out.println(split[i]+"-"+split[j]+"-"+split[k]);
                            System.out.println(allRole.getRoleName());
                            continue;
                        }
                        if (hashMap.get(tagThree) == null) {
                            hashMap.put(tagThree, new ArrayList<>(Collections.singleton(allRole.getRarity() + "_" + allRole.getRoleName())));
                        } else {
                            List<String> roleNames = new ArrayList<>(hashMap.get(tagThree));
                            roleNames.add(allRole.getRarity() + "_" + allRole.getRoleName());
                            hashMap.put(tagThree, roleNames);
                        }
                    }
                }
            }


//            System.out.println(tags);


        }

        List<RecResultVo> voList = new ArrayList<>();
        Set<String> keySet = hashMap.keySet();
        for (String key : keySet) {
            RecResultVo recResultVo = new RecResultVo();
            recResultVo.setTags(key.replace("_,", ""));
            List<Object> roleNames = new ArrayList<>(hashMap.get(key));
            LinkedHashSet<Object> hashSet = new LinkedHashSet<>(roleNames);
            recResultVo.setResult(new ArrayList<>(hashSet));
            recResultVo.setLessRarity(hashMap.get(key).get(hashMap.get(key).size() - 1).split("_")[0]);
            voList.add(recResultVo);
        }

        voList.sort(comparing(RecResultVo::getLessRarity).reversed());


        return voList;
    }

    @Override
    public List<RoleTagData> findAll() {

        List<RoleTagData> byTypeAndRarityBetween = roleTagDataDao.findByTypeAndRarityBetween(0, 1, 6);
        return byTypeAndRarityBetween;
    }
}


//    @Override
//    public List<RecResultVo> findAllByTypeAndRarityNew(Integer type, String[] reqTags, Integer rarityMin) {
//
//
//
//        int r1Type = 3;
//        int r6Type = 5;
//        for (int i = 0; i < reqTags.length; i++) {
////            System.out.println(reqTags[i]+" , ");
//            if ("高级资深干员".equals(reqTags[i])) {
//                r6Type = 6;
//            }
//
//        }
//        r1Type  = rarityMin;
//
//        List<RoleTagData> allData = roleTagDataDao.findByTypeAndRarityBetween(type,r1Type,r6Type);
//
//
//        HashMap<String, List<String>> hashMap = new HashMap<>();
//        for (int i = 0; i < allData.size(); i++) {
//            String tags = "_";
//            for (int j = 0; j < reqTags.length; j++) {
//                if (reqTags[j].equals(allData.get(i).getTag1())) {
//                    tags = tags+","+reqTags[j];
//                } else if (reqTags[j].equals(allData.get(i).getTag2())) {
//                    tags = tags+","+reqTags[j];
//                } else if (reqTags[j].equals(allData.get(i).getTag3())) {
//                    tags = tags+","+reqTags[j];
//                } else if (reqTags[j].equals(allData.get(i).getTag4())) {
//                    tags = tags+","+reqTags[j];
//                } else if (reqTags[j].equals(allData.get(i).getTag5())) {
//                    tags = tags+","+reqTags[j];
//                } else if (reqTags[j].equals(allData.get(i).getTag6())) {
//                    tags = tags+","+reqTags[j];
//                }
//            }
//
//            if("_".equals(tags)){
//                continue;
//            }
//
//
//            System.out.println(tags);
////            String[] tagItem = tags.split(",");
////            for (int j = 1; j < tagItem.length; j++) {
////                System.out.println("跳出");
////                if(tagItem.length<4) continue;
////               String  tagit = "_,"+tagItem[j];
////                if(hashMap.get(tagit)==null){
////                    hashMap.put(tagit,new ArrayList<>(Collections.singleton( allData.get(i).getRarity() + "_"+ allData.get(i).getRoleName())));
////                }else {
////                    List<String> roleNames = new ArrayList<>(hashMap.get(tagit));
////                    roleNames.add(allData.get(i).getRarity() + "_"+ allData.get(i).getRoleName());
////                    hashMap.put(tagit,roleNames);
////                }
////            }
//
//            if(hashMap.get(tags)==null){
//                hashMap.put(tags,new ArrayList<>(Collections.singleton( allData.get(i).getRarity() + "_"+ allData.get(i).getRoleName())));
//            }else {
//                List<String> roleNames = new ArrayList<>(hashMap.get(tags));
//                roleNames.add(allData.get(i).getRarity() + "_"+ allData.get(i).getRoleName());
//                hashMap.put(tags,roleNames);
//            }
//
//        }
//
//        List<RecResultVo> voList= new ArrayList<>();
//        Set<String> keySet = hashMap.keySet();
//        for(String key:keySet){
//            RecResultVo recResultVo = new RecResultVo();
//            recResultVo.setTags(key.replace("_,",""));
//            List<Object> roleNames = new ArrayList<>(hashMap.get(key));
//            recResultVo.setResult(roleNames);
//            recResultVo.setLessRarity(hashMap.get(key).get(hashMap.get(key).size()-1).split("_")[0]);
//            voList.add(recResultVo);
//        }
//
//        voList.sort(comparing(RecResultVo::getLessRarity).reversed());
//
//
//        return voList;
//    }