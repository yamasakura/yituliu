package com.briup.service.impl;

import com.briup.bean.IpData;
import com.briup.bean.Visits;
import com.briup.bean.dataBean.Stage;
import com.briup.bean.vo.StageResultVo;
import com.briup.common.exception.ServiceException;
import com.briup.common.util.IpUtil;
import com.briup.common.util.ResultCode;
import com.briup.dao.*;
import com.briup.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private VisitsDao visitsDao;

    @Autowired
    private  StageResultVoDao stageResultVoDao;

    @Autowired
    private  StageDao stageDao;

    @Autowired
    private IpDataDao ipDataDao;

    @Override
    public List<StageResultVo> findByMainNotNull() {
        List<StageResultVo> page = stageResultVoDao.findByTypeNotNullOrderByEfficiencyDesc();
        return page;
    }


    @Override
    public List<Object> findByEffAndIsShow(Integer ends, Double efficiency) {
        double start = System.currentTimeMillis();

        Integer pageNum = 0;
        Integer pageSize = 7;
        String[] mainName = new String[]{"全新装置", "异铁组", "轻锰矿", "凝胶", "扭转醇", "酮凝集组", "RMA70-12", "炽合金", "研磨石", "糖组",
                "聚酸酯组", "晶体元件", "固源岩组", "半自然溶剂", "化合切削液"};

        List<Object> pageList = new ArrayList<>();
        for (int i = 0; i < mainName.length; i++) {
            Page<StageResultVo> pageCopy = findDataSelectByTypeByEfficiencyDesc(
                    mainName[i], 1, efficiency, pageNum, pageSize);
            List<StageResultVo> pageArr = pageCopy.getContent();
            List<StageResultVo> page = new ArrayList<>(pageArr);

            try {
                for (int k = 0; k < pageArr.size(); k++) {
                    if (page.get(k).getIsUseValue() == 0) {
                        StageResultVo stageResultVo = new StageResultVo();
                        stageResultVo.setItemId(page.get(k).getItemId());
                        stageResultVo.setStageName(page.get(k).getStageName());
                        stageResultVo.setTimes(page.get(k).getTimes());
                        stageResultVo.setProbability(page.get(k).getProbability());
                        stageResultVo.setExpect(page.get(k).getExpect());
                        stageResultVo.setEfficiency(page.get(k).getEfficiency() + 0.1);
                        stageResultVo.setMain(page.get(k).getMain());
                        stageResultVo.setType(page.get(k).getType());
                        stageResultVo.setSecondary("龙门币");
                        stageResultVo.setSecondaryId("4001");
                        stageResultVo.setColor(-1);
                        stageResultVo.setUpdateDate(page.get(k).getUpdateDate());
                        stageResultVo.setSpm(page.get(k).getSpm());
                        stageResultVo.setIsShow(page.get(k).getIsShow());
                        page.add(0, stageResultVo);
                        k = k + 1;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            for (int k = 0; k < page.size(); k++) {
                Double biaozhunben = pageArr.get(0).getEfficiency();
                String stageName = pageArr.get(0).getStageName();
                if (pageArr.get(0).getIsUseValue() == 0) {
                    biaozhunben = pageArr.get(1).getEfficiency();
                }
                Double percentage = page.get(k).getEfficiency() / biaozhunben;
                DecimalFormat dfbfb = new DecimalFormat("0.0");
                page.get(k).setPercentage(Double.valueOf(dfbfb.format(percentage * 100)));
            }
            pageList.add(page);

        }

        double end = System.currentTimeMillis();
        System.out.println("查找用时:---" + (end - start)  + "ms");

        return pageList;
    }

    @Override
    public Page<StageResultVo> findDataSelectByTypeByEfficiencyDesc(String main, Integer ends, Double efficiency, Integer pageNum,
                                                                  Integer pageSize) {
        if (main != null && pageNum != null && pageSize != null) {
            Pageable pageable = PageRequest.of(pageNum, pageSize);
            Page<StageResultVo> page = stageResultVoDao.findByTypeAndIsShowAndEfficiencyGreaterThanOrderByEfficiencyDesc(main, ends, efficiency, pageable);
            if (page != null) {
                return page;
            } else {
                throw new ServiceException(ResultCode.DATA_NONE);
            }
        } else {
            throw new ServiceException(ResultCode.PARAM_IS_BLANK);
        }
    }




	@Override
	public List<Object> selectVisits(String begin, String end, HttpServletRequest request) {
        Visits visits = new Visits();
        Visits visits1 = new Visits();
        SimpleDateFormat daydf = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
        String today = daydf.format(new Date());
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE,-1);
        String yesterday =  daydf.format(calendar.getTime());



        Visits yesterdayDate = visitsDao.findByVisitsDay(yesterday);
        Visits todayDate = visitsDao.findByVisitsDay(today);
        int todayVisits = 1;
        int yesterdayVisits = yesterdayDate.getVisits();
        if(todayDate!=null){
         todayVisits =  todayDate.getVisits()+1;
       }else {
           yesterdayVisits = yesterdayVisits/2;
            visits1.setVisitsDay(yesterday);
            visits1.setVisits(yesterdayVisits);
            visitsDao.save(visits1);
       }



        visits.setVisitsDay(today);
        visits.setVisits(todayVisits);
//        System.out.println("访问次数"+visits.getVisits());
        visitsDao.save(visits);



		List<Visits> all = visitsDao.findAll();
        List<String> dayList = new ArrayList<>();
        List<Integer> visitList = new ArrayList<>();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date date = ft.parse(begin);
            Long beginDate =  date.getTime();
            date = ft.parse(end);
            Long endDate =  date.getTime();


            for(int i=0;i<all.size();i++){
                Date now = ft.parse(all.get(i).getVisitsDay());
                Long nowDate =  now.getTime();

                if (beginDate<nowDate&&nowDate<endDate){
                    dayList.add(all.get(i).getVisitsDay());
                    visitList.add(all.get(i).getVisits());
                };
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        List<Object>  visitVo = new ArrayList<>();
        visitVo.add(dayList);
        visitVo.add(visitList);


        String ipAddress = null;
        try {
            ipAddress = IpUtil.getIpAddress(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
        IpData ipData = new IpData();

        if ((ipDataDao.findIpDataByIpAndVisitTime(ipAddress, today) ==null)){
            ipData.setId(ipAddress+new Date().getTime());
            ipData.setIp(ipAddress);
            ipData.setVisitTime(today);
            ipDataDao.save(ipData);
        }



        return visitVo;
	}


    @Override
    public List<StageResultVo> findByStageId(String stageId) {

        List<StageResultVo> byStageId = stageResultVoDao.findByStageId(stageId);


        return byStageId;
    }

    @Override
    public void updateStageInfo(Integer isShow,String stageId) {

        stageDao.updataStageInfo(isShow,stageId);


    }

    @Override
    public List<Stage> findAllStageInfo() {

        List<Stage> all = stageDao.findAllByStageNameNotNullOrderByChapterNameDesc();
        return all;
    }

}
