package com.briup.service;

import com.briup.bean.StoreCostPer;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface StoreCostPerService {

	void save(List<StoreCostPer> storeCostPer);
	//更新商店性价比信息

	void update(MultipartFile file);

	//查找商店性价比信息
	List<StoreCostPer> findAll(String type);





}
