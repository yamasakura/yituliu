package com.briup.service;

import com.briup.bean.vo.RecResultVo;
import com.briup.bean.dataBean.RoleTagData;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface RoleTagDataService {

    void saveAll(MultipartFile file);



    void exportData(HttpServletResponse response);

    List<RecResultVo> findAllByTypeAndRarityNew(Integer type, String[] tags,Integer rarityMax);

    List<RoleTagData> findAll();
}
