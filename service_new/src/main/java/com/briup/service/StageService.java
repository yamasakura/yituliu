package com.briup.service;

import com.briup.bean.dataBean.Stage;
import com.briup.bean.vo.StageVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface StageService {



	List<Stage> findAll();

    List<StageVo> findAllEx();

    void importStageData(MultipartFile file);

    void exportStageData(HttpServletResponse response);
}
