package com.briup.service;

import com.briup.bean.dataBean.QuantileTable;

import java.util.List;

public interface QuantileTableService {

    List<QuantileTable> findAll();
}
