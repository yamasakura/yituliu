package com.briup.service;


import com.briup.bean.gacha.GachaData;
import com.briup.bean.gacha.GachaVo;
import com.briup.bean.gacha.MutableInteger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface GachaService {

    Map<String ,String> saveGachaData(String token);

   GachaVo findById(String id);
   void test();

    HashMap<String, Integer> findAllData (String uid);
}
