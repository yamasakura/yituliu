package com.briup.service;

import com.briup.bean.gacha.UserData;

import java.util.List;

public interface UserDataService {

    void saveAll(List<UserData> userDataList);
}
