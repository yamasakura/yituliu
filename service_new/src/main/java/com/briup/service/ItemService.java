package com.briup.service;

import com.briup.bean.ItemRevise;
import com.briup.bean.dataBean.Item;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

public interface ItemService {

	void item();

	List<Item> findAll();

	List<ItemRevise> findAllItemRevise();

	List<ItemRevise> itemResvise(HashMap<String, Double> desc);

	void exportItemData(HttpServletResponse response);
}
