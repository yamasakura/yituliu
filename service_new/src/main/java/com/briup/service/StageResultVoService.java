package com.briup.service;

import com.briup.bean.PenguinData;
import com.briup.bean.vo.StageResultVo;

import java.util.List;

public interface StageResultVoService {

	//保存关卡效率信息
	void saveAll(List<StageResultVo> stageResultVos);

	//删除所有数据
	void deleteInBatch();

	//保存关卡效率信息
	List<PenguinData> stageResult(Integer type);

}
