package com.briup.service;

import com.briup.bean.Visits;
import com.briup.bean.dataBean.Stage;
import com.briup.bean.vo.StageResultVo;
import org.springframework.data.domain.Page;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;


public interface CategoryService {


	List<Object> findByEffAndIsShow(Integer ends,Double efficiency);
	
	Page<StageResultVo> findDataSelectByTypeByEfficiencyDesc(String main, Integer ends, Double efficiency , Integer pageNum, Integer pageSize);

	List<StageResultVo> findByMainNotNull();

	List<Object> selectVisits(String begin, String end, HttpServletRequest request);

	List<StageResultVo> findByStageId(String stageId);

	void updateStageInfo(Integer isShow,String stageId);

	List<Stage> findAllStageInfo();
}
